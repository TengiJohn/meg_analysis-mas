function customize_Commit_Mycallback(handles)
alglist=get_MAS('algorithm_test');
newalg.name=get(handles.edit1,'string');
newalg.date=get(handles.text5,'string');
newalg.author=get(handles.text7,'string');
newalg.comment=get(handles.edit4,'string');
if(strcmp(get_Customize_var('newOrEdit'),'edit'))
   alglist{get_MEG_Analysis_var('ALGIndex')}=newalg;
else
   alglist=[alglist;newalg];
end
 set_MAS('algorithm_test',alglist);

fighandles=get_MAS('fighandles');
MEG_Analysis=fighandles.MEG_Analysis;
disp_alg(MEG_Analysis);
ALG_Select_Mycallback(MEG_Analysis);
 
end

