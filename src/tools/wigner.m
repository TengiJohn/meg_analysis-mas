%default complex signal
function w=wigner(data,varargin)
L=100;
NewData=[zeros(1,L) data zeros(1,L)];

optargin=size(varargin,2);
stdargin=nargin-optargin;

if(optargin==0)
    mode='complex';
else
    mode=varargin{1};
end

switch mode
    case 'real' 
        DATA=NewData;
    case 'complex'
        DATA=hilbert(NewData);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f=zeros(2*L-1,length(data));
for n=1:length(data)
    for l=1:L
        f(l,n)=DATA(n+L+l)*conj(DATA(n+L-l));
    end
    for l=L:2*L-1
        f(l,n)=DATA(n+l-L)*conj(DATA(n-l+3*L));
    end
end
w=fft(f)/(2*L-1);

%each column corresponds to each time point
end

