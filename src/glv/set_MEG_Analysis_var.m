function set_MEG_Analysis_var(varargin)
optargin=size(varargin,2);
if(optargin==1)
    setappdata(0,'MEG_Analysis_var',[]);
end
if(optargin==1)
    setappdata(0,'MEG_Analysis_var',varargin{1});
else
    temp=getappdata(0,'MEG_Analysis_var');
    for i=1:2:optargin-1
        switch(varargin{i})
            case 'runtimeALG'
                temp.runtimeALG=varargin{i+1};
            case 'ALGIndex'
                temp.ALGIndex=varargin{i+1};
        end
    end
    setappdata(0,'MEG_Analysis_var',temp);
end

end

