function Filepath=File_Open_Mycallback()
[filename,pathname,filterindex]=uigetfile({'*.fif','RAWfile (*.fif)'},'Select a file','MultiSelect','off','../Data/somebody.fif');
Filepath=[];
if(filename~=0)
    Filepath=[pathname filename];
    set_fif_info('filename',Filepath);
    Close_fif();
    Open_fif();
    set_fif_info('samplefrequency',GET_samplefrequency(),'channelnames',GET_channelnames());
    set_filter('samplefrequency',GET_samplefrequency());
 
    raw=get_mne_raw();
    chan_names=raw.info.ch_names;
    
    MEG_num=0;
    EEG_num=0;
    
    temp=strfind(chan_names,'MEG');
    for i=1:length(temp)
        if ~isempty(temp{i})
            MEG_num=MEG_num+1;
            MEG_names{MEG_num}=chan_names{i};
        end
    end
    set_fif_info('MEGChannelNum',MEG_num);
    set_fif_info('MEGChannelNames',MEG_names);
    
    temp=strfind(chan_names,'EEG');
    EEG_names={};
    for i=1:length(temp)
        if ~isempty(temp{i})
            EEG_num=EEG_num+1;
            EEG_names{EEG_num}=chan_names{i};
        end
    end
    
    set_fif_info('EEGChannelNum',EEG_num);
    set_fif_info('EEGChannelNames',EEG_names);
    
    set_fif_info('totletime',(raw.last_samp-raw.first_samp+1)/1000);    
    
    
    Data_display;
end
end

