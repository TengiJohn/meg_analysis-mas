function set_Data_display_var(varargin)
optargin=size(varargin,2);
stdargin=nargin-optargin;

if(optargin==1)
    setappdata(0,'Data_display_var',varargin{1});
elseif(optargin>1)
    par=getappdata(0,'Data_display_var');
    for i=1:2:optargin-1
        switch varargin{i}
            case 'timebuffer'
                par.timebuffer=varargin{i+1};
            case 'data_displaying'
                par.data_displaying=varargin{i+1};
            case 'channelname'
                par.channelname=varargin{i+1};
            case 'realtime'
                par.realtime=varargin{i+1};
            case 'datamat'
                par.datamat=varargin{i+1};
            case 'ptime'
                par.ptime=varargin{i+1};
            case 'pchannel'
                par.pchannel=varargin{i+1};
            case 'leftpoint'
                par.leftpoint=varargin{i+1};
            case 'rightpoint'
                par.rightpoint=varargin{i+1};
            case 'rtime'
                par.rtime=varargin{i+1};
            case 'dataframe'
                par.dataframe=varargin{i+1};
            case 'currenttrainobj'
                par.currenttrainobj=varargin{i+1};
            case 'trainset_data'
                par.trainset_data=varargin{i+1};
            case 'fdatamat'
                par.fdatamat=varargin{i+1};
            case 'leftedge'
                par.leftedge=varargin{i+1};
            case 'rightedge'
                par.rightedge=varargin{i+1};
            case 'frame_rtime'
                par.frame_rtime=varargin{i+1};
            case 'avgpower'
                par.avgpower=varargin{i+1};
            case 'avgfreq'
                par.avgfreq=varargin{i+1};
        end
    end
    setappdata(0,'Data_display_var',par);
end
end

