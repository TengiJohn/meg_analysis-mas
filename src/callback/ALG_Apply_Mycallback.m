function ALG_Apply_Mycallback(handles)
tmp=get_MEG_Analysis_var('runtimeALG');
alg=str2func(tmp.name);

data=get_Data_display_var('data_displaying');
time=get_Data_display_var('realtime');
f=get_fif_info('samplefrequency');
tb=get_Data_display_var('timebuffer');
tr=linspace(time,time+tb,length(data));
channelname=get_Data_display_var('channelname');

output=alg(data);

if(~isempty(output))
    figure;
    subplot(3,1,1);
    plot(tr,data);
    title(channelname);
    
    subplot(3,1,2);
    plot(1:length(output.detail),output.detail);
    
    subplot(3,1,3)
    stem(1:length(output.classified),output.classified);
    title(strcat('threshold',num2str(output.threshold)));
    ylim([0 1.5]);
end
end

