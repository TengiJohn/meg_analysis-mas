function varargout = Customize(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Customize_OpeningFcn, ...
                   'gui_OutputFcn',  @Customize_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

function Customize_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
ini_UI_Customize(handles);
guidata(hObject, handles);
end

function varargout = Customize_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;

end

function edit2_Callback(hObject, eventdata, handles)
end
function edit2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function edit1_Callback(hObject, eventdata, handles)
end
function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function pushbutton1_Callback(hObject, eventdata, handles)
customize_Commit_Mycallback(handles);
end

function edit4_Callback(hObject, eventdata, handles)
end

function edit4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function pushbutton2_Callback(hObject, eventdata, handles)
close
end


function figure1_CloseRequestFcn(hObject, eventdata, handles)
delete(hObject);
end
