function set_TrainSet_View_var(varargin)
optargin=size(varargin,2);
stdargin=nargin-optargin;
if(optargin==0)
    setappdata(0,'TrainSet_View_var',[]);
elseif(optargin==1)
    setappdata(0,'TrainSet_View_var',varargin{1});
elseif(optargin>1)
    par=getappdata(0,'TrainSet_View_var');
    for i=1:2:optargin-1
        switch varargin{i}
            case 'filename'
                par.filename=varargin{i+1};
            case 'trainset'
                par.trainset=varargin{i+1};
            case 'index'
                par.index=varargin{i+1};
            case 'raw'
                par.raw=varargin{i+1};
            case 'currenttime'
                par.currenttime=varargin{i+1};
            case 'data_displaying'
                par.data_displaying=varargin{i+1};
        end
    end
    setappdata(0,'TrainSet_View_var',par);
end
end

