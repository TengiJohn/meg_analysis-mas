function set_MAS(varargin)
if(size(varargin,2)==0)
    setappdata(0,'MAS',[]);
elseif(size(varargin,2)==1)
    setappdata(0,'MAS',varargin{1});
else
    temp=getappdata(0,'MAS');
    for i=1:2:size(varargin,2)-1
        switch varargin{i}
            case 'user'
                temp.user=varargin{i+1};
            case 'algorithm_test'
                temp.algorithm_test=varargin{i+1};
            case 'fighandles'
                temp.fighandles=varargin{i+1};
            case 'trainset'
                temp.trainset=varargin{i+1};
            case 'topography'
                temp.topography=varargin{i+1};
            case 'want_eeg'
                temp.want_eeg=varargin{i+1};
            case 'want_meg'
                temp.want_meg=varargin{i+1};
            case 'want_sti'
                temp.want_sti=varargin{i+1};
        end
    end
    setappdata(0,'MAS',temp);
end
 
end

