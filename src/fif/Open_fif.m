function Open_fif()
me='MAS��Open_fif';
filename=get_fif_info('filename');
% rawdata('any',filename);
try
   raw = fiff_setup_read_raw(filename);
catch
    try 
        raw=fiff_setup_read_raw(filename,1);
    catch 
        error(me,'%s',mne_omit_first_line(lasterr));
    end
end

set_mne_raw(raw);
end

