function ch_names = GET_channelnames()
raw=get_mne_raw();
picks = 1:raw.info.nchan; 
ch_names   = raw.info.ch_names(picks);

end

