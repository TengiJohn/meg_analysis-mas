function disp_histogram(hist1,hist2,num)
if(hist2==1)
data2=GET_rawchannel(get_Data_display_var('pchannel'));
if(get_filter('enable'))
    data2=bandpass(data2);
end
figure('Name','Current channel')
hist(data2,num);
end
if(hist1==1)
    data1=get_Data_display_var('data_displaying');
    figure('Name','Current window')
    hist(data1,num);
end
end

