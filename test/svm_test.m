function svm_test()
%test sigma
figure('Name','test sigma')
load fisheriris; xdata = meas(51:end,1:2); group = species(51:end);

sigma = 1;svmStruct = svmtrain(xdata,group,'kernel_function','rbf','rbf_sigma', sigma,'showplot',true);

hold on;

sigma = 2;svmStruct = svmtrain(xdata,group,'kernel_function','rbf','rbf_sigma', sigma,'showplot',true);

hold on;

sigma = 4;svmStruct = svmtrain(xdata,group,'kernel_function','rbf','rbf_sigma', sigma,'showplot',true);

%test C
figure('Name','test C')

load fisheriris; xdata = meas(51:end,1:2); group = species(51:end);

C = 2^0;svmStruct = svmtrain(xdata,group,'kernel_function','rbf','boxconstraint', C,'showplot',true);

hold on;

C = 2^3;svmStruct = svmtrain(xdata,group,'kernel_function','rbf','boxconstraint', C,'showplot',true);

hold on;

C = 2^6;svmStruct = svmtrain(xdata,group,'kernel_function','rbf','boxconstraint', C,'showplot',true);


end

