% my matlab path:
% C:\Program Files\MATLAB\R2011b
function Setup(env)
me='MAS:Setup';

if (nargin==0)
    env='win64';
end
switch env
    case 'win32'
        disp('Setting up for your matlab windows-32bit environmet...');
        addpath(genpath([pwd '/src']),'-frozen');
        addpath([pwd '/lib/FIFFACCESS/Windows-32/meg_pd_1.2'],'-frozen');
        addpath(genpath([pwd '/ui']),'-frozen');
        %addpath([pwd '/lib/libsvm-3.11/matlab'],'-frozen');
        addpath(genpath([pwd '/test']),'-frozen');
        addpath(genpath([pwd '/temp']),'-frozen');     
        copyfile([pwd '/lib/FIFFACCESS/Windows-32/lib/i686-pc-cygwin/libmagnet_pd_1.2.dll'],[matlabroot '/bin/win32/libmagnet_pd_1.2.dll'],'f'); 
    case 'win64'
        disp('Setting up for your matlab windows-64bit environmet...');
        %copyfile([pwd '/lib/Wavelab850/'],[matlabroot '/toolbox/Wavelab850/'],'f'); 
        %addpath([pwd '/lib/Wavelab850'],'-frozen');
        addpath(genpath([pwd '/src']),'-frozen');
        addpath([pwd '/lib/MNE/'],'-frozen');
        addpath(genpath([pwd '/ui']),'-frozen');
        %addpath([pwd '/lib/libsvm-3.11/matlab'],'-frozen');
        addpath(genpath([pwd '/test']),'-frozen');
        addpath(genpath([pwd '/database']),'-frozen');
        addpath(genpath([pwd '/lib/YAMLMatlab/']),'-frozen');
        addpath(genpath([pwd '/cmd']),'-frozen');
        addpath(genpath([pwd '/cmd']),'-frozen');
        %WavePath;
        %InstallMEX;
    case 'linux-32-matlab7'
    case 'linux-32-matlab6'
    case 'linux-64'
    otherwise
        disp('The environmet variable you put not supported yet.');
        disp('The list of supported environmet variable:');
        disp('win32');
        disp('win64');
        disp('linux-32-matlab7');
        disp('linux-32-matlab6');
        disp('linux-64');
end
savepath;
end

