function set_MASPower_var( varargin )
optargin=size(varargin,2);
stdargin=nargin-optargin;

if(optargin==1)
    setappdata(0,'MASPower_var',varargin{1});
elseif(optargin>1)
    par=getappdata(0,'MASPower_var');
    for i=1:2:optargin-1
        switch varargin{i}
            case 'handles'
                par.handles=varargin{i+1};
            case 'begintime'
                par.begintime=varargin{i+1};
            case 'timelength'
                par.timelength=varargin{i+1};
            case 'datatype' %0-MEG 1-EEG 2-MOG
                par.datatype=varargin{i+1};
            case 'sensortype'%0-Mag_1 1-Mag_2 2-Gra
                par.sensortype=varargin{i+1};
            case 'datamat'
                par.datamat=varargin{i+1};
            case 'avgfreq'
                par.avgfreq=varargin{i+1};
            case 'avgpower'
                par.avgpower=varargin{i+1};
            case 'eegbuttongroup'
                par.eegbuttongroup=varargin{i+1};
            case 'selecteddata'
                par.selecteddata=varargin{i+1};
            case 'megbuttongroup'
                par.megbuttongroup=varargin{i+1};
            case 'selectedchannels'
                par.selectedchannels=varargin{i+1};
            case 'dispchannels'
                par.dispchannels=varargin{i+1};
        end
    end
    setappdata(0,'MASPower_var',par);
end

end

