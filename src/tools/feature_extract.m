function feature = feature_extract(from,to,grounddata)
%window data must be included in the ground data
fs=get_fif_info('samplefrequency');
feature=[];
%apply a window with size 100 according to max amplitude
bpground=bandpass(grounddata);
[a,center]=max(abs(bpground(from:to)));
center=center+from-1;
from=center-50;
to=center+50;

background_from=center-500;
background_to=center+500;

if(background_from<1)
    background_from=1;
    background_to=101;
end
if(background_to>length(grounddata))
    background_to=length(grounddata);
    background_from=background_to-100;
end


if(from<1)
    from=1;
    to=101;
end
if to>length(bpground)
    to=length(bpground);
    from=to-100;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
normground=bpground/norm(bpground);
data=normground(from:to);
width=length(data);
buffer=length(normground);

%cwt
scalerange=20:100;
wname='db4';
coefs=cwt(bpground,scalerange,wname);
coefsmap=zeros(size(coefs));
for i=1:size(coefs,1)
    coefsmap(i,:)=coefs(i,:).^2/sum(coefs(i,:).^2);
end

% choose center as the absoultem apex
[m,center]=max(abs(normground));

cwtenergymap=coefsmap(:,center);

cwtenergymap_mean=mean(cwtenergymap);
cwtenergymap_std=std(cwtenergymap);

%feature cwtenergymap mean and std
feature.cwtenergymap_mean=cwtenergymap_mean;
feature.cwtenergymap_std=cwtenergymap_std;

%stft
seqlen=buffer;
NFFT=2^nextpow2(seqlen);                       %FFT length
winwidth=200;                                  %window width
wintrans=5;                                    %window translation step
nframes=floor((seqlen-winwidth)/wintrans)+1;   %window frame number

%window=rectwin(100);                                 %rectangular window
%window=gausswin(winwidth+1,2.5);                     %gaussian window
%window=bartlett(winwidth+1);                          %triangular window
window=hamming(winwidth+1);                           %hamming window

Ytwz=zeros(NFFT/2+1,nframes);

for i=1:nframes
    temp=normground;
    xl=(i-1)*wintrans+2;
    xh=xl+winwidth;
    if(xh>length(temp)-1)
        xh=length(temp)-1;
    end

    temp(1:xl-1)=0;
    temp(xh+1:end)=0;
    temp(xl:xh)=temp(xl:xh).*window(1:xh-xl+1)';
    
    ytwz=2*abs(fft(temp,NFFT)/length(temp));
    Ytwz(:,i)=ytwz(1:NFFT/2+1);
end

fr=fs/2*linspace(0,1,NFFT/2+1);

fmin=10;
fmax=50;
flow=1;
fhigh=length(fr);

for i=1:length(fr)
    if fr(i)<=fmin
        flow=i;
    end
    if fr(length(fr)-i+1)>=fmax
        fhigh=length(fr)-i+1;
    end
end
Ytwz_cat=Ytwz(flow:fhigh,:);

Ytwz_cat=flipud(Ytwz_cat);
stftmap=sum(Ytwz_cat.^2,1)/norm(Ytwz_cat)^2;

maxenergyratio=0;
for i=1:nframes
    xl=(i-1)*wintrans+2;
    xh=xl+winwidth;
    if center<xh&&center>xl
        if stftmap(i)>maxenergyratio
            maxenergyratio=stftmap(i);
        end
    end
end

stftmap_maxenergyratio=maxenergyratio;

%feature stftmap_maxenergyratio
feature.stftmap_maxenergyratio=stftmap_maxenergyratio;

%time domain feature


end

