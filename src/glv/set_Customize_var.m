function set_Customize_var(varargin)
res=getappdata(0,'Customize_var');
if(size(varargin,2)==0)
    res=[];
elseif(size(varargin,2)==1)
    res=varargin{1};
else
    for i=1:2:size(varargin,2)-1
        switch varargin{i}
            case 'newOrEdit'
                res.newOrEdit=varargin{i+1};
        end 
    end
end
setappdata(0,'Customize_var',res);
end

