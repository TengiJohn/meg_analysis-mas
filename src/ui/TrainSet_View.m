function varargout = TrainSet_View(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TrainSet_View_OpeningFcn, ...
                   'gui_OutputFcn',  @TrainSet_View_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

function TrainSet_View_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
set_TrainSet_View_var('filename',[],'trainset',[],'index',0);
guidata(hObject, handles);
end

function varargout = TrainSet_View_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;
end

function pushbutton1_Callback(hObject, eventdata, handles)
if(~isempty(get_TrainSet_View_var('filename')))
    temp=get_TrainSet_View_var('index');
    trainset=get_TrainSet_View_var('trainset');
    data=trainset.data;
    
    new=temp-1;
    if(new<1)
        new=1;
    end
    
    set_TrainSet_View_var('index',new);
    
    oldfile=data{temp}.rawinfo.filename;
    newfile=data{new}.rawinfo.filename;
    
    if ~strcmp(oldfile,newfile)
        raw = fiff_setup_read_raw(newfile);
        set_TrainSet_View_var('raw',raw);
    end
    set_TrainSet_View_var('currenttime',data{new}.rawinfo.begintime);
    raw=get_TrainSet_View_var('raw');
    buffer=data{new}.rawinfo.timelength;
    channel=data{new}.rawinfo.channelindex;
    t0=data{new}.rawinfo.begintime;
    datamat=GET_datamat_time(t0,buffer,raw);
    background=datamat(channel,:);
    set_TrainSet_View_var('data_displaying',bandpass(background));
    
    disp_trainset(handles);
end
end

function pushbutton2_Callback(hObject, eventdata, handles)
if(~isempty(get_TrainSet_View_var('filename')))
    temp=get_TrainSet_View_var('index');
    trainset=get_TrainSet_View_var('trainset');
    data=trainset.data;
    
    new=temp+1;
    if(new>length(data))
        new=length(data);
    end
    
    set_TrainSet_View_var('index',new);
    
    oldfile=data{temp}.rawinfo.filename;
    newfile=data{new}.rawinfo.filename;
    
    if ~strcmp(oldfile,newfile)
        raw = fiff_setup_read_raw(newfile);
        set_TrainSet_View_var('raw',raw);
    end
    
    set_TrainSet_View_var('currenttime',data{new}.rawinfo.begintime);
    
    raw=get_TrainSet_View_var('raw');
    buffer=data{new}.rawinfo.timelength;
    channel=data{new}.rawinfo.channelindex;
    t0=data{new}.rawinfo.begintime;
    datamat=GET_datamat_time(t0,buffer,raw);
    background=datamat(channel,:);
    set_TrainSet_View_var('data_displaying',bandpass(background));
    disp_trainset(handles);
end
end

function uipushtool1_ClickedCallback(hObject, eventdata, handles)
[filename,pathname,filterindex]=uigetfile({'*.tset','TrainSetfile (*.tset)'},'Select a train set file','./database/trainset/trainset.tset');
fp=strcat(pathname,filename);
if(filename~=0)
    set_TrainSet_View_var('filename',fp);
    switch filterindex
        case 1
            ts=ReadYaml(fp);
            if(~isempty(ts))
                set_TrainSet_View_var('trainset',ts);
                set_TrainSet_View_var('index',1);
                set_TrainSet_View_var('currenttime',ts.data{1}.rawinfo.begintime);
                raw = fiff_setup_read_raw(ts.data{1}.rawinfo.filename);
                set_TrainSet_View_var('raw',raw);
                data=ts.data;
                buffer=data{1}.rawinfo.timelength;
                channel=data{1}.rawinfo.channelindex;
                t0=data{1}.rawinfo.begintime;
                datamat=GET_datamat_time(t0,buffer,raw);
                background=datamat(channel,:);
                set_TrainSet_View_var('data_displaying',bandpass(background));
                
                trainset=get_TrainSet_View_var('trainset');
                st=trainset.savetime;
                
                set(handles.text6,'string',trainset.user.name);
                set(handles.text9,'string',strcat(num2str(st{1}),'-',num2str(st{2}),'-',num2str(st{3}),'-',num2str(st{4}),':',num2str(st{5})));
                set(handles.text11,'string',num2str(length(data)));
                set(handles.text15,'string',get_TrainSet_View_var('filename'));
                set(handles.text16,'string',raw.info.filename);
                countPositive=0;
                countNegative=0;
                for i=1:length(data)
                    if data{i}.class==1
                        countPositive=countPositive+1;
                    else
                        countNegative=countNegative+1;
                    end
                end
                set(handles.text21,'string',num2str(countPositive));
                set(handles.text23,'string',num2str(countNegative));
                disp_trainset(handles);
            end
    end
end
end

function uipushtool2_ClickedCallback(hObject, eventdata, handles)
temp=get_TrainSet_View_var('trainset');
saveTrainSet(temp.data);
end

function axes1_ButtonDownFcn(hObject, eventdata, handles)
cp=get(hObject,'CurrentPoint');
x=get(hObject,'xlim');
t=cp(1,1);
if t<x(1)
    t=x(1);
elseif t>x(2)
    t=x(2);
end
set_TrainSet_View_var('currenttime',t);
disp_trainset(handles);
end


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
end

function File_Open_Callback(hObject, eventdata, handles)
uipushtool1_ClickedCallback(hObject, eventdata, handles);
end

function File_Save_Callback(hObject, eventdata, handles)
end

function File_Export_Callback(hObject, eventdata, handles)
num=get_TrainSet_View_var('index');
[filename,pathname,filterindex]=uiputfile({'*.bmp';'*.jpg'},'Save axis as',strcat('./userdata/','sample',num2str(num),'.bmp'));
fp=strcat(pathname,filename);
if(filename~=0)
    pix=getframe(handles.axes1);
    imwrite(pix.cdata,fp);
end

end


% --------------------------------------------------------------------
function uipushtool3_ClickedCallback(hObject, eventdata, handles)
saveSnapShot();
end

function uipanel6_SelectionChangeFcn(hObject, eventdata, handles)
index=get_TrainSet_View_var('index');
ts=get_TrainSet_View_var('trainset');
if ~isempty(ts)
    data=ts.data;
    currentobject=data{index};
    switch get(eventdata.NewValue,'Tag')
        case 'radiobutton1'
            currentobject.class=1;
        case 'radiobutton2'
            currentobject.class=-1;
    end
    data{index}=currentobject;
    ts.data=data;
    set_TrainSet_View_var('trainset',ts);
    disp_trainset(handles);
end
end
