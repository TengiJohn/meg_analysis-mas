function y = get_TrainSet_View_var(varargin)
me='MAS:get_TrainSet_View_var';
if(size(varargin,2)>1)
    error(me,'Too many input arguments!');
end
temp=getappdata(0,'TrainSet_View_var');
if(size(varargin,2)==0)
    y=getappdata(0,'TrainSet_View_var');
else
    switch(varargin{1})
        case 'filename'
            y=temp.filename;
        case 'trainset'
            y=temp.trainset;
        case 'index'
            y=temp.index;
        case 'raw';
            y=temp.raw;
        case 'currenttime'
            y=temp.currenttime;
        case 'data_displaying'
            y=temp.data_displaying;
    end
end


end

