function y=get_filter(varargin)
num=size(varargin,2);
temp=getappdata(0,'Filter');
if(num==0)
    y=temp;
else
    switch varargin{1}
        case 'passband'
            y=temp.passband;
        case 'stopband'
            y=temp.stopband;
        case 'rp'
            y=temp.rp;
        case 'rs'
            y=temp.rs;
        case 'type'
            y=temp.type;
        case 'samplefrequency'
            y=temp.samplefrequency;
        case 'design'
            y=temp.design;
        case 'bpnumerator'
            y=temp.bpnumerator;
        case 'bpdenumerator'
            y=temp.bpdenumerator;
        case 'bpenable'
            y=temp.bpenable;
        case 'mode'
            y=temp.mode;
        case 'freqcenter'
            y=temp.freqcenter;
        case 'qfactor'
            y=temp.qfactor;
        case 'notchmode'
            y=temp.notchmode;
        case 'notchnumerator'
            y=temp.notchnumerator;
        case 'notchdenumerator'
            y=temp.notchdenumerator;
        case 'notchenable'
            y=temp.notchenable;
    end
end
end

