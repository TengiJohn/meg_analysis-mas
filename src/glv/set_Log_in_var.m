function set_Log_in_var(varargin)
num=size(varargin,2);
if num==0
    setappdata(0,'Log_in_var',[]);
else
    temp=getappdata(0,'Log_in_var');
    for i=1:2:num-1
        switch varargin{i}
            case 'previous_user'
                temp.previous_user=varargin{i+1};
            case 'authen'
                temp.authen=varargin{i+1};
        end
    end
    setappdata(0,'Log_in_var',temp);
end
end

