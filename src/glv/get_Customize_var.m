function y=get_Customize_var(varargin)
temp=getappdata(0,'Customize_var');
if(size(varargin,2)==0)
    y=temp;
else
    switch(varargin{1})
        case 'newOrEdit'
            y=temp.newOrEdit;
    end
end
end

