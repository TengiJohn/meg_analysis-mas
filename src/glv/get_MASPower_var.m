function y = get_MASPower_var( varargin )
optargin=size(varargin,2);
stdargin=nargin-optargin;
var=getappdata(0,'MASPower_var');
if(optargin==0)
    y=var;
else
    switch varargin{1}
        case 'handles'
            y=var.handles;
        case 'begintime'
            y=var.begintime;
        case 'timelength'
            y=var.timelength;
        case 'datatype' %0-MEG 1-EEG 2-MOG
            y=var.datatype;
        case 'sensortype' %0-Mag_1 1-Mag_2 2-Gra
            y=var.sensortype;
        case 'datamat'
            y=var.datamat;
        case 'avgfreq'
            y=var.avgfreq;
        case 'avgpower'
            y=var.avgpower;
        case 'eegbuttongroup'
            y=var.eegbuttongroup;
        case 'selecteddata'
            y=var.selecteddata;
        case 'megbuttongroup'
            y=var.megbuttongroup;
        case 'selectedchannels'
            y=var.selectedchannels;
        case 'dispchannels'
            y=var.dispchannels;
    end
end
end

