function avg_stft(data,fmin,fmax,bt,tl)

avg_stft=0;
seqlen=size(data,2);
winwidth=200;                        %window width
wintrans=100;                       %window translation step
nframes=floor((seqlen-winwidth)/wintrans)+1;   %window frame number
%window=rectwin(winwidth+1);                                 %rectangular window
window=gausswin(winwidth+1,2.5);                     %gaussian window
%window=bartlett(winwidth+1);                          %triangular window
%window=window.*hamming(winwidth+1);                           %hamming window
NFFT=2^nextpow2(seqlen);                       %FFT length

for pchannel=1:size(data,1)
Ytwz=zeros(NFFT/2+1,nframes);
for i=1:nframes
    temp=data(pchannel,:);
    xl=(i-1)*wintrans+2;
    xh=xl+winwidth;
    if(xh>length(temp)-1)
        xh=length(temp)-1;
    end

    temp(1:xl-1)=0;
    temp(xh+1:end)=0;
    temp(xl:xh)=temp(xl:xh).*window(1:xh-xl+1)';
    
    ytwz=2*abs(fft(temp,NFFT)/length(temp));
    Ytwz(:,i)=ytwz(1:NFFT/2+1);
end

fr=1000/2*linspace(0,1,NFFT/2+1);
flow=1;
fhigh=length(fr);

for i=1:length(fr)
    if fr(i)<=fmin
        flow=i;
    end
    if fr(length(fr)-i+1)>=fmax
        fhigh=length(fr)-i+1;
    end
end

Ytwz_cat=Ytwz(flow:fhigh,:);

Ytwz_cat=Ytwz_cat/norm(Ytwz_cat);

avg_stft=Ytwz_cat+avg_stft;
end
figure;
finaldata=avg_stft;
downx=ceil(size(finaldata,2)/1000);
downy=ceil(size(finaldata,1)/1000);
finaldata=finaldata(1:downx:end,1:downy:end);

finaldata=finaldata/norm(finaldata);

Ybound=[fmin,fmax];%,size(finaldata,2));
Xbound=[bt,bt+tl];%,size(finaldata,1));
imagesc(Xbound,Ybound,finaldata);
axis xy;
% [X,Y]=meshgrid(Xbound,Ybound);
% surface(X,Y,finaldata);
% legend
ylim([fmin,fmax]);
xlim([bt,bt+tl]);
xlabel('time(s)');
ylabel('frequency(Hz)');
% set(gca,'XTickLabel',{num2str(linspace(fmin,fmax,size(finaldata,2)))});
colormap(jet);
end

