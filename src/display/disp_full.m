function disp_full(handles)
switch get(handles.uipanel1,'selectedobject')
    case handles.radiobutton1
        beginnum=3;
    case handles.radiobutton2
        beginnum=2;
    case handles.radiobutton3
        beginnum=1;
end

fdatamat=get_Data_display_var('fdatamat');

top=get_MAS('topography');
switch get(handles.uipanel20,'selectedobject')
    case handles.radiobutton4
        left=cell2mat(top.lefttemporal_index);
        leftdata=fdatamat(left(beginnum,:),:);
        right=cell2mat(top.righttemporal_index);
        rightdata=fdatamat(right(beginnum,:),:);
    case handles.radiobutton5
        left=cell2mat(top.leftparietal_index);
        leftdata=fdatamat(left(beginnum,:),:);
        right=cell2mat(top.rightparietal_index);
        rightdata=fdatamat(right(beginnum,:),:);
    case handles.radiobutton6
        left=cell2mat(top.leftoccipital_index);
        leftdata=fdatamat(left(beginnum,:),:);
        right=cell2mat(top.rightoccipital_index);
        rightdata=fdatamat(right(beginnum,:),:);
    case handles.radiobutton7
        left=cell2mat(top.leftfrontal_index);
        leftdata=fdatamat(left(beginnum,:),:);
        right=cell2mat(top.rightfrontal_index);
        rightdata=fdatamat(right(beginnum,:),:);
end
leftpartition=draw_block(handles.axes1,handles.axes3,leftdata);

rightpartition=draw_block(handles.axes2,handles.axes4,rightdata);

set_FullChannel_View_var('leftindex',left(beginnum,:));
set_FullChannel_View_var('rightindex',right(beginnum,:));
set_FullChannel_View_var('leftpartition',leftpartition);
set_FullChannel_View_var('rightpartition',rightpartition);

end

function y=draw_block(axes1,axes2,data)
datamat=get_Data_display_var('datamat');
t0=double(get_Data_display_var('realtime'));
tb=get_Data_display_var('timebuffer');
tr=linspace(t0,t0+tb,size(datamat,2));

Amax=max(max(data));
Amin=min(min(data));
axes(axes1);
cla;
yoffset=0.5*(Amax-Amin);
ymax=Amax;
ymin=Amin-(size(data,1)-1)*yoffset;
for i=1:size(data,1)
    temp=data(i,:)-(i-1)*yoffset;
    h=line(tr,temp);
    y(i,:)=[max(temp) min(temp)];
    set(h,'HitTest','off');
    xlim([tr(1),tr(end)]);
    if ymin*ymax~=0
    ylim([ymin,ymax]);
    end
    hold on;
end

axes(axes2);
cla
for i=1:size(data,1)
    h=line(tr,data(i,:)-mean(data(i,:)));
    set(h,'HitTest','off');
    xlim([tr(1),tr(end)]);
    if Amin*Amax~=0
    ylim([Amin,Amax]);
    end
    hold on;
end

set(axes1,'xticklabel','');
set(axes1,'yticklabel','');
set(axes2,'xticklabel','');
set(axes2,'yticklabel','');
end

