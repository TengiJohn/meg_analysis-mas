%1-D fft
function disp_fft(fdata,Fs,fmin,fmax)
NFFT = 2^nextpow2(length(fdata));
Y = fft(fdata,NFFT)/length(fdata);
f = Fs/2*linspace(0,1,NFFT/2+1);
A=2*abs(Y(1:NFFT/2+1));
if(nargin==2)
    plot(f,A);
elseif(nargin==4)
    FImin=1;
    FImax=length(f);
    for i=1:length(f)
        if f(i)<=fmin
            FImin=i;
        end
        if f(length(f)-i+1)>=fmax
            FImax=length(f)-i+1;
        end
    end
    plot(f(FImin:FImax),A(FImin:FImax));   
    xlabel('frequency(Hz)');
    ylabel('magnitude of one-side fft');
end

