function ini_UI_Customize(handles)
time=clock;
time=[num2str(time(1)) '-' num2str(time(2)) '-' num2str(time(3))];
set(handles.text5,'string',time);
namestr=[];
user=get_MAS('user');
authorstr=user.name;
comment=[];
if(strcmp(get_Customize_var('newOrEdit'),'edit'))
    tmp=get_MEG_Analysis_var('runtimeALG');
    namestr=tmp.name;
    authorstr=tmp.author;
    comment=tmp.comment;
end

set(handles.edit1,'string',namestr);
set(handles.text7,'string',authorstr);
set(handles.edit4,'string',comment);

end

