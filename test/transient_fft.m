function y = transient_fft(data)
y=[];

w1=1;
w2=2;
t1=1:0.01:10;
t2=1:0.01:1.2;
s1=cos(2*pi*w1*t1);
s2=cos(2*pi*w1*t1)+[zeros(1,700) 10*cos(2*pi*w2*t2) zeros(1,180)];
subplot(2,2,1)
plot(t1,s1);
title('purely 1Hz background wave ');
subplot(2,2,2)
disp_fft(s1,100,0,5);
title('its frequency-amplitude map');

subplot(2,2,3)
plot(t1,s2);
title('1Hz background wave with 3Hz transient noise');
subplot(2,2,4)
disp_fft(s2,100,0,5);
title('its frequency-amplitude map');

end

