function disp_frame(handles)
lp=get_Data_display_var('leftpoint');
rp=get_Data_display_var('rightpoint');
data=get_Data_display_var('data_displaying');
cn=get_Data_display_var('channelname');

width=rp-lp+1;
t0=str2double(get(handles.text39,'string'));
t1=str2double(get(handles.text40,'string'));

set(handles.uipanel10,'Title',cn);
axes(handles.axes2);
cla
set_Data_display_var('dataframe',data(lp:rp));
xlim([t0 t1]);
h=line(linspace(t0,t1,width),data(lp:rp));
set(h,'HitTest','off');

left=get_Data_display_var('leftedge');
h=line([left left],get(gca,'ylim'),'color','k','linestyle',':');
set(h,'HitTest','off');

right=get_Data_display_var('rightedge');
h=line([right right],get(gca,'ylim'),'color','k','linestyle',':');
set(h,'HitTest','off');

end

