function MASPower_megspecify_Mycallback(handles)

sensor=get_MASPower_var('sensortype');

megnum=get_fif_info('MEGChannelNum');
megnames_total=get_fif_info('MEGChannelNames');
count=1;

for i=1:megnum
    name=megnames_total{i};
    if str2double(name(end))==sensor
        megnames{count}=name;
        channels(count)=i;
        count=count+1;
    end
end
 
megnum=count-1;

rownum=round(megnum^0.5);
colnum=floor(megnum/rownum)+1;
f=figure('Name','Select','menubar','none','units','pixels','position',...
    [60 60 (colnum-1)*80 rownum*50+40],'resize','off');
h=uibuttongroup('visible','off','Units','normalized','Position',[0 0 1 1]);


for i=1:megnum
    u(i) = uicontrol('Style','checkbox','String',megnames{i},...
        'units','pixels','position',...
        [mod(i-1,rownum)*80 rownum*50-floor((i-1)/rownum)*50 80 40],...
        'parent',h,'HandleVisibility','off','value',0);
end

conf=uicontrol('style','pushbutton','string','ok',...
    'units','pixels','position',[colnum*40-40 10 50 20],...
    'callback',@MEG_select_callback);
set(h,'Visible','on');

set_MASPower_var('megbuttongroup',u);
set_MASPower_var('dispchannels',channels);
end

function MEG_select_callback(hObject,eventdata)
set(hObject,'enable','off');

MASPower_handles=get_MASPower_var('handles');
len=str2double(get(MASPower_handles.edit1,'string'));
if len>get_fif_info('totletime');
    len=get_fif_info('totletime');
end
begintime=str2double(get(MASPower_handles.edit3,'string'));

[datamat,times]=GET_datamat_time(begintime,len);


u=get_MASPower_var('megbuttongroup');
channels=get_MASPower_var('dispchannels');

c=1;
for i=1:length(u)
    if get(u(i),'value')
        selectedchannels(c)=channels(i);
        c=c+1;
    end
end

data=datamat(selectedchannels,:);
data=masfilter(data);

set_MASPower_var('begintime',begintime,'timelength',len,'datatype',...
    'MEG','datamat',datamat,'selecteddata',data,...
    'selectedchannels',selectedchannels);

set(hObject,'enable','on');
end


