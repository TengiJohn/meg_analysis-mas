function y = STFT_test(data)
% short time fourier transform test
y=[];

%energy normalization
normalConst=sqrt(sum(data.^2));
data=data/normalConst;
%

channelname=get_Data_display_var('channelname');
t=get_Data_display_var('realtime');
t=10;

len=50;        %data length
% len=get_fif_info('totletime');

fs=get_fif_info('samplefrequency');
datamat=GET_datamat_time(t,len);
avg_stft=0;
for pchannel=1:3:306
% pchannel=get_Data_display_var('pchannel');
% full_channel=zeros(1,length(data));
% for i=pchannel:pchannel+10
%     temp=bandpass(datamat(i,:));
%     %temp=temp/norm(temp);
%     full_channel=full_channel+temp;
% end
% figure;
% subplot(2,1,1)
% plot(linspace(t,t+len,length(data)),data);
% subplot(2,1,2)
% plot(linspace(t,t+len,length(full_channel)),full_channel);


% figure;
% subplot(3,1,1);
% disp_fft(rawdata,fs,0,50);
% title(strcat('rawdata',channelname,' t0: ',num2str(t),' timebuffer: ',num2str(len)));
% subplot(3,1,2);
% disp_fft(data,fs,0,50);
% title('bandpassed data');
% subplot(3,1,3);
% disp_fft(full_channel,fs,0,50);
% title('full channel');

seqlen=round(len*fs);
NFFT=2^nextpow2(seqlen);                       %FFT length
winwidth=500;                        %window width
wintrans=250;                       %window translation step
nframes=floor((seqlen-winwidth)/wintrans)+1;   %window frame number

%window=rectwin(winwidth+1);                                 %rectangular window
window=gausswin(winwidth+1,2.5);                     %gaussian window
%window=bartlett(winwidth+1);                          %triangular window
%window=window.*hamming(winwidth+1);                           %hamming window

Ytwz=zeros(NFFT/2+1,nframes);

for i=1:nframes
    temp=datamat(pchannel,:);
    xl=(i-1)*wintrans+2;
    xh=xl+winwidth;
    if(xh>length(temp)-1)
        xh=length(temp)-1;
    end

    temp(1:xl-1)=0;
    temp(xh+1:end)=0;
    temp(xl:xh)=temp(xl:xh).*window(1:xh-xl+1)';
    
    ytwz=2*abs(fft(temp,NFFT)/length(temp));
    Ytwz(:,i)=ytwz(1:NFFT/2+1);
    %display short time fft
%     figure;
%     plot(linspace(0,fs/2,length(ytwz)),ytwz);
%     title(strcat(channelname,'stft ',' frame: ',num2str(i),' window size: ',num2str(winwidth)));
end

fr=fs/2*linspace(0,1,NFFT/2+1);

fmin=0;
fmax=35;
flow=1;
fhigh=length(fr);

for i=1:length(fr)
    if fr(i)<=fmin
        flow=i;
    end
    if fr(length(fr)-i+1)>=fmax
        fhigh=length(fr)-i+1;
    end
end

[X,Y]=meshgrid(1:nframes,fr(flow:fhigh));

Ytwz_cat=Ytwz(flow:fhigh,:);

Ytwz_cat=Ytwz_cat/norm(Ytwz_cat);

avg_stft=Ytwz_cat+avg_stft;
% figure;
% surf(X,Y,Ytwz_cat);
% colormap(jet);
% 
% xlabel('time frame number');
% ylabel('frequency domain (Hz)');
% zlabel('time-frequency amplitude');
% title(strcat(channelname,' starting time: ',num2str(t),' s; ','frame width: ',num2str(winwidth),' ms; ',...
%      'translation step: ',num2str(wintrans),' ms;'));
end
figure;
imagesc(flipud(avg_stft'));
% legend
ylabel('time');
xlabel('frequency Hz');
colormap(jet);
 
% detail=sum(Ytwz_cat.^2,1)/sum(sum(Ytwz_cat.^2));
% y.detail=detail;
% classified=zeros(1,size(Ytwz_cat,2));
% 
% threshold=0.003;
% for i=1:length(classified)
%     if detail(i)>threshold
%         classified(i)=1;
%     end
% end
% y.threshold=threshold;
% y.classified=classified;

end

