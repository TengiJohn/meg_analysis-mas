function ini_UI_Filter(hObject,handles)
passband=get_filter('passband');
stopband=get_filter('stopband');
Rp=get_filter('rp');
Rs=get_filter('rs');
type=get_filter('type');
notchmode=get_filter('notchmode');
freqcenter=get_filter('freqcenter');
qfactor=get_filter('qfactor');
bpenable=get_filter('bpenable');
notchenable=get_filter('notchenable');

set(handles.popupmenu1,'Value',type);

set(handles.edit1,'String',num2str(passband(1)));
set(handles.edit3,'String',num2str(passband(2)));
set(handles.edit5,'String',num2str(stopband(1)));
set(handles.edit6,'String',num2str(stopband(2)));

set(handles.edit7,'String',num2str(Rp));
set(handles.edit8,'String',num2str(Rs));

set(handles.edit9,'String',num2str(freqcenter));
set(handles.edit10,'String',num2str(qfactor));

set(handles.radiobutton2,'value',notchmode);
set(handles.radiobutton4,'value',notchenable);
set(handles.radiobutton5,'value',bpenable);

guidata(hObject,handles);
end

