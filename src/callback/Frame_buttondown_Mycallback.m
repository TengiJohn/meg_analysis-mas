function Frame_buttondown_Mycallback(hObject,handles)
disp_frame(handles);
fs=get_fif_info('samplefrequency');
t=get(hObject,'CurrentPoint');
t=t(1,1);
t=round(t*fs)/fs;
y=get(hObject,'YLim');
data=get_Data_display_var('dataframe');
t0=str2double(get(handles.text39,'string'));
fs=get_fif_info('samplefrequency');
index=round((t-t0)*fs+1);
if(index>length(data))
    index=length(data);
end

h=line([t t],y(1:2),'color','r');
set(h,'HitTest','off');
set(handles.text43,'string',strcat(num2str(t),' s'));
set_Data_display_var('frame_rtime',t);
set(handles.text44,'string',strcat(num2str(data(index)),' ft/cm'));

end

