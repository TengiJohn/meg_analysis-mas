function y = cat_brainfunc(data)
y=[];
lefttemporal={'MEG011x','MEG014x','MEG013x','MEG021x','MEG022x','MEG151x','MEG024x','MEG023x',...
    'MEG154x','MEG152x','MEG161x','MEG162x','MEG153x'};
righttemporal={'MEG142x','MEG131x','MEG132x','MEG144x','MEG143x','MEG134x','MEG133x','MEG261x',...
    'MEG241x','MEG242x','MEG264x','MEG262x','MEG263x'};
leftparietal={'MEG041x','MEG042x','MEG063x','MEG044x','MEG043x','MEG071x','MEG181x','MEG182x',...
    'MEG074x','MEG163x','MEG184x','MEG183x','MEG201x'};
rightparietal={'MEG104x','MEG111x','MEG112x','MEG072x','MEG114x','MEG113x','MEG073x','MEG221x',...
    'MEG222x','MEG224x','MEG202x','MEG223x','MEG244x'};
leftoccipital={'MEG172x','MEG164x','MEG191x','MEG204x','MEG194x','MEG171x','MEG173x','MEG192x',...
    'MEG211x','MEG174x','MEG193x','MEG214x'};
rightoccipital={'MEG203x','MEG231x','MEG243x','MEG234x','MEG232x','MEG252x','MEG251x','MEG253x',...
    'MEG212x','MEG233x','MEG254x','MEG213x'};
leftfrontal={'MEG052x','MEG051x','MEG031x','MEG054x','MEG053x','MEG082x','MEG012x','MEG034x',...
    'MEG032x','MEG033x','MEG061x','MEG062x','MEG064x'};
rightfrontal={'MEG081x','MEG091x','MEG092x','MEG094x','MEG101x','MEG102x','MEG093x','MEG121x',...
    'MEG103x','MEG124x','MEG123x','MEG122x','MEG141x'};

cns=get_fif_info('channelnames');

lefttemporal_index=search(cns,lefttemporal);
righttemporal_index=search(cns,righttemporal);
leftparietal_index=search(cns,leftparietal);
rightparietal_index=search(cns,rightparietal);
leftoccipital_index=search(cns,leftoccipital);
rightoccipital_index=search(cns,rightoccipital);
leftfrontal_index=search(cns,leftfrontal);
rightfrontal_index=search(cns,rightfrontal);

disp.lefttemporal_index=lefttemporal_index;
disp.lefttemporal=lefttemporal;
disp.righttemporal_index=righttemporal_index;
disp.righttemporal=righttemporal;
disp.leftparietal_index=leftparietal_index;
disp.leftparietal=leftparietal;
disp.rightparietal_index=rightparietal_index;
disp.rightparietal=rightparietal;
disp.leftoccipital_index=leftoccipital_index;
disp.leftoccipital=leftoccipital;
disp.rightoccipital_index=rightoccipital_index;
disp.rightoccipital=rightoccipital;
disp.leftfrontal_index=leftfrontal_index;
disp.leftfrontal=leftfrontal;
disp.rightfrontal_index=rightfrontal_index;
disp.rightfrontal=rightfrontal;

WriteYaml('./database/topography/brainregion.top',disp);

end

function y=search(cns,names)
    s_1=[];
    s_2=[];
    s_3=[];
    for i=1:306
        for k=1:length(names)
            tmp1=cns{i};
            tmp2=names{k};
            if strcmp(tmp1,strcat(tmp2(1:6),'1'))
                s_1=[s_1,i];
            elseif strcmp(tmp1,strcat(tmp2(1:6),'2'))
                s_2=[s_2 i];
            elseif strcmp(tmp1,strcat(tmp2(1:6),'3'))
                s_3=[s_3 i];
            end
        end
    end
    y(1,:)=s_1;
    y(2,:)=s_2;
    y(3,:)=s_3;
end