function out=masfilter(data)
switch get_filter('mode')
    case 0
        out=data';
        if(get_filter('bpenable'))
        bpb=get_filter('bpnumerator');
        bpa=get_filter('bpdenumerator');
        out=filter(bpb,bpa,out);
        end
        
        if(get_filter('notchenable'))
        nb=get_filter('notchnumerator');
        ba=get_filter('notchdenumerator');
        out=filter(nb,ba,out);
        end
        
        out=out';
    case 1
        funcname=get_filter('design');
        f=str2func(funcname); 
        out=filter(f(),data');
        out=out';
end
end
