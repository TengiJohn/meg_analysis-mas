function ts=loadTrainSet()
[filename,pathname,filterindex]=uigetfile({'*.tset','TrainSetfile (*.tset)'},'Select a file','MultiSelect','off','./database/trainset/trainset_0.tset');
fp=strcat(pathname,filename);
temp=[];
if(filename~=0)
    switch filterindex
        case 1
            temp=ReadYaml(fp);
    end
end
set_MAS('trainset',temp);
ts=temp;

end

