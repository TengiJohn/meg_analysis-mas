function disp_trainset(handles)
axes(handles.axes1);
trainset=get_TrainSet_View_var('trainset');
index=get_TrainSet_View_var('index');
tn=get_TrainSet_View_var('currenttime');
raw=get_TrainSet_View_var('raw');
background=get_TrainSet_View_var('data_displaying');
data=trainset.data;
t0=data{index}.rawinfo.begintime;
buffer=data{index}.rawinfo.timelength;
from=data{index}.rawinfo.from;
to=data{index}.rawinfo.to;
channel=data{index}.rawinfo.channelindex;
channelname=raw.info.ch_names(channel);

tr=linspace(t0,t0+buffer,length(background));
cla;
w=[-max(abs(background)) max(abs(background))];
if(data{index}.class==1)
    color='r';
else
    color='g';
end
h=rectangle('position',[tr(from),w(1),tr(to)-tr(from),w(2)-w(1)],'facecolor',color);
xlim([t0,t0+buffer]);
ylim(w);
set(h,'HitTest','off');
hold on;

h=line(tr,background);
xlim([t0,t0+buffer]);
ylim(w);
set(h,'HitTest','off');
hold on;

h=line([tn tn],w,'color','r');
set(h,'HitTest','off');


pointer=round((tn-t0)/buffer*length(background))+1;
if(pointer>length(background))
    pointer=length(background);
end

set(handles.text12,'string',num2str(index));
set(handles.text3,'string',channelname);
set(handles.text1,'string',strcat(num2str(tn),' s'));
set(handles.text2,'string',strcat(num2str(background(pointer)),' ft/cm'));

if(data{index}.class==1)
    set(handles.uipanel6,'selectedobject',handles.radiobutton1);
else
    set(handles.uipanel6,'selectedobject',handles.radiobutton2);
end
end

