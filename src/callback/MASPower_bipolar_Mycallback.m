function MASPower_bipolar_Mycallback( handles )
eegnum=get_fif_info('EEGChannelNum');
eegnames=get_fif_info('EEGChannelNames');

rownum=round(eegnum^0.5);
colnum=floor(eegnum/rownum)+1;
f=figure('Name','Biploar','menubar','none','units','pixels',...
    'position',[100 100 colnum*100 rownum*100],'resize','off');
h=uibuttongroup('visible','off','Units','normalized','Position',[0 0 1 1]);
for i=1:eegnum
    u(i) = uicontrol('Style','checkbox','String',eegnames{i},...
        'Units','normalized','position',...
        [mod(i-1,rownum)/rownum+0.05 1-floor(i/rownum)/colnum-0.1 0.2 0.1],...
        'parent',h,'HandleVisibility','off','value',0,...
        'callback',@Bipolar_checkbox_callback);
end

conf=uicontrol('style','pushbutton','string','ok',...
    'units','pixels','position',[colnum*50-40 10 50 20],...
    'callback',@EEG_bipolar_callback);
set(h,'Visible','on');

set_MASPower_var('eegbuttongroup',u);
end

function EEG_bipolar_callback(hObject,eventdata)
set(hObject,'enable','off');

MASPower_handles=get_MASPower_var('handles');
len=str2double(get(MASPower_handles.edit1,'string'));
if len>get_fif_info('totletime');
    len=get_fif_info('totletime');
end
begintime=str2double(get(MASPower_handles.edit3,'string'));

[datamat,times]=GET_datamat_time(begintime,len);

u=get_MASPower_var('eegbuttongroup');
EEG_names=get_fif_info('EEGChannelNames');
MEG_num=get_fif_info('MEGChannelNum');

for i=1:length(u)
    v(i)=get(u(i),'value');
    n{i}=get(u(i),'string');
end

count=1;
for i=1:length(u)
    if v(i)
        for j=1:length(EEG_names)
            if(strcmp(EEG_names{j},n{i}))
                channels(count)=MEG_num+j;
                count=count+1;
            end
        end
    end
end
set_MASPower_var('selectedchannels',channels);
data=datamat(channels(1),:)-datamat(channels(2),:);
data=masfilter(data);

set_MASPower_var('begintime',begintime,'timelength',len,'datatype',...
    'EEG','datamat',datamat,'selecteddata',data,...
    'selectedchannels',channels);

set(hObject,'enable','on');
end

function Bipolar_checkbox_callback(hObject,eventdata)
u=get_MASPower_var('eegbuttongroup');
numofon=0;
for i=1:length(u)
    if get(u(i),'value')
        numofon=numofon+1;
    end
    set(u(i),'enable','on');
end

if numofon==2
    for i=1:length(u)
        if ~get(u(i),'value')
            set(u(i),'enable','off');
        end
    end
end

end

