function nextpage(handles)
rt=get_Data_display_var('realtime');
tb=get_Data_display_var('timebuffer');
temp=rt+tb;
if (temp+tb)>get_fif_info('totletime')
    temp=get_fif_info('totletime')-tb;
end
set_Data_display_var('realtime',temp);
rt=get_Data_display_var('realtime');
[B,times]=GET_datamat_time(rt,tb);
set_Data_display_var('datamat',B);

set_Data_display_var('fdatamat',masfilter(B));

if(get(handles.checkbox1,'value')==1)
    [a,I1]=max(abs(B));
    [a,I2]=max(a);
    max_chan=I1(I2);
    set_Data_display_var('pchannel',max_chan);
end

disp_chans(handles);

temp=get_MAS('fighandles');
disp_full(temp.FullChannel_View);


end

