function disp_chans(handles)
axes(handles.axes1);

sf=double(get_fif_info('samplefrequency'));
Pchan=get_Data_display_var('pchannel');

eeg_names=get_fif_info('EEGChannelNames');
meg_names=get_fif_info('MEGChannelNames');
eeg_num=get_fif_info('EEGChannelNum');
meg_num=get_fif_info('MEGChannelNum');

if(Pchan>meg_num)
    Channame=eeg_names{Pchan-meg_num};
else
    Channame=meg_names{Pchan};
end
    
if rem(Pchan,3)==0
    unit=' ft/cm';
else
    unit=' ft';
end
tb=double(get_Data_display_var('timebuffer'));
datamat=get_Data_display_var('datamat');
T=double(get_Data_display_var('realtime'));
totlechannel=get_fif_info('totlechannel');
timerange=linspace(T,T+tb-1/sf,size(datamat,2));

pt=get_Data_display_var('ptime');

rt=T+(pt-1)/sf;

set_Data_display_var('rtime',rt);
set_Data_display_var('channelname',Channame);

set(handles.text3,'String',[num2str(rt) ' S']);
set(handles.text1,'String',num2str(Pchan));
set(handles.text2,'String',Channame);

set(handles.time_slider,'Max',round(tb*sf));
set(handles.time_slider,'Sliderstep',[1/(tb*sf-1) 10/(tb*sf-1)]);
set(handles.time_slider,'Value',pt);
set(handles.channel_slider,'Value',totlechannel-Pchan+1);

data_displaying=masfilter(datamat(Pchan,:));

set_Data_display_var('data_displaying',data_displaying)

cla;
h=line(timerange,data_displaying);
set(h,'HitTest','off');

avg=mean(data_displaying);
set(handles.baseline,'string',num2str(avg));
h=line([timerange(1),timerange(end)],[avg,avg],'color','k','linestyle',':');
set(h,'HitTest','off');

h=line([rt rt],get(handles.axes1,'YLim'),'color','r');
set(h,'HitTest','off');

if Pchan<=get_fif_info('MEGChannelNum') 
    set(handles.text4,'String',[num2str(data_displaying(pt)) unit]);
else
    set(handles.text4,'String',[num2str(data_displaying(pt)) unit]);
end

[avgpower,avgfreq]=poweranalysis(data_displaying,'avgpower','avgfreq');

set_Data_display_var('avgpower',avgpower,'avgfreq',avgfreq);

set(handles.text53,'String',strcat('avgpower: ',num2str(avgpower),'          avgfreq: ',num2str(avgfreq),' Hz'));
hold off

end

