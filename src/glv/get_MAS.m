function y=get_MAS(varargin)
temp=getappdata(0,'MAS');
if(size(varargin,2)==0)
    y=temp;
else
    switch varargin{1}
        case 'user'
            y=temp.user;
        case 'algorithm_test'
            y=temp.algorithm_test;
        case 'fighandles'
            y=temp.fighandles;
        case 'trainset'
            y=temp.trainset;
        case 'topography'
            y=temp.topography;
        case 'want_eeg'
            y=temp.want_eeg;
        case 'want_meg'
            y=temp.want_meg;
        case 'want_sti'
            y=temp.want_sti;
    end
end
    
end

