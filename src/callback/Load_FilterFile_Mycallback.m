function Load_FilterFile_Mycallback(hObject,eventdata,handles)
[filename,pathname,filterindex]=uigetfile({'*.m'},'Select a file','MultiSelect','off',[pwd '/temp/filter/default.m']);
if(filename~=0)
      filename=filename(1:length(filename)-2);
      set_filter('mode',1);
      set_filter('design',filename);
end

guidata(hObject,handles);
end

