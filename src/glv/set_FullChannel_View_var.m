function set_FullChannel_View_var(varargin)
optargin=size(varargin,2);
stdargin=nargin-optargin;

if(optargin==1)
    setappdata(0,'FullChannel_View_var',varargin{1});
elseif(optargin>1)
    par=getappdata(0,'FullChannel_View_var');
    for i=1:2:optargin-1
        switch varargin{i}
            case 'rightpartition'
                par.rightpartition=varargin{i+1};
            case 'leftpartition'
                par.leftpartition=varargin{i+1};
            case 'leftindex'
                par.leftindex=varargin{i+1};
            case 'rightindex'
                par.rightindex=varargin{i+1};
        end
    end
    setappdata(0,'FullChannel_View_var',par);
end
end

