function varargout = Filter(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Filter_OpeningFcn, ...
                   'gui_OutputFcn',  @Filter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end
function Filter_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;

ini_UI_Filter(hObject,handles);

guidata(hObject, handles);
end
function varargout = Filter_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;
end
function popupmenu1_Callback(hObject, eventdata, handles)
set_filter('type',get(hObject,'Value'));
end
function popupmenu1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function edit1_Callback(hObject, eventdata, handles)
temp=get_filter('passband');
set_filter('passband',[str2double(get(hObject,'String')) temp(2)]);
end

function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edit3_Callback(hObject, eventdata, handles)
temp=get_filter('passband');
set_filter('passband',[temp(1) str2double(get(hObject,'String'))]);
end
function edit3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

function edit5_Callback(hObject, eventdata, handles)
temp=get_filter('stopband');
set_filter('stopband',[str2double(get(hObject,'String')) temp(2)]);
end
function edit5_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edit6_Callback(hObject, eventdata, handles)
temp=get_filter('stopband');
set_filter('stopband',[temp(1) str2double(get(hObject,'String'))]);
end

function edit6_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function edit7_Callback(hObject, eventdata, handles)
set_filter('rp',str2double(get(hObject,'String')));
end
function edit7_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function edit8_Callback(hObject, eventdata, handles)
set_filter('rs',str2double(get(hObject,'String')));
end

function edit8_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function pushbutton1_Callback(hObject, eventdata, handles)
Filter_configure_Mycallback(hObject,handles);
end

function edit9_Callback(hObject, eventdata, handles)
end

function edit9_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edit10_Callback(hObject, eventdata, handles)
end

function edit10_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
end

function pushbutton3_Callback(hObject, eventdata, handles)
end
function radiobutton4_Callback(hObject, eventdata, handles)
end

function radiobutton5_Callback(hObject, eventdata, handles)
end
