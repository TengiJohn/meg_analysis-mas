function ini_UI_Data_display(hObject,handles)
temp=struct();
setappdata(0,'Data_display_var',temp);
set_Data_display_var('timebuffer',5);
set_Data_display_var('data_displaying',[],'channelname',[],'realtime',0);
set_Data_display_var('datamat',[],'ptime',1,'pchannel',1,'channelbuffer',1);
set_Data_display_var('trainset_data',[]);
Close_fif();
Open_fif();

rt=get_Data_display_var('realtime');
tb=get_Data_display_var('timebuffer');
[B,times]=GET_datamat_time(rt,rt+tb);
set_Data_display_var('datamat',B);

s1=size(B,1);
set_fif_info('totlechannel',s1);

set(handles.channel_slider,'Max',s1);
set(handles.channel_slider,'Min',1);
set(handles.channel_slider,'Value',s1);
set(handles.channel_slider,'Sliderstep',[1/(s1-1) 5/(s1-1)]);

set(handles.time_slider,'Min',1);

set(handles.edit1,'String',num2str(get_Data_display_var('pchannel')));
set(handles.edit3,'String',num2str(get_Data_display_var('timebuffer')));
set(handles.edit7,'String','1');

set(handles.text47,'string','0');
set(handles.text48,'string','0');
set(handles.text52,'string',get_fif_info('filename'));

set(handles.text5,'String',[num2str(get_fif_info('samplefrequency')) 'Hz']);
set(handles.text36,'string',['0:' num2str(get_fif_info('totletime')) ' s']);

set(handles.axes1,'ButtonDownFcn',{@Data_display_buttondown_Mycallback,handles,'0'});

set_Data_display_var('fdatamat',masfilter(B));
disp_chans(handles);

fighandles=get_MAS('fighandles');
fighandles.Data_display=handles;
set_MAS('fighandles',fighandles);

FullChannel_View;

end

