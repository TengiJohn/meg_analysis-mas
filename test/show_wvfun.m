function show_wvfun()
wname='coif3';
num=5;
[wp,x]=wpfun(wname,5,num);
%figure('position',[0,0,200,600]);

dispnum=3;
for i=0:dispnum
    %subplot(dispnum+1,1,i+1);
    figure;
    tr=round(length(x)/5*2.25):round(length(x)/5*2.7);
    plot(x,wp(i+1,:));
    title(strcat(wname,' wf for s=',num2str(2^-i),' with ',num2str(length(x)),' points'));
    axis off;
    %title(num2str(dispnum-i+1));
end

end

