function y=get_MEG_Analysis_var(varargin)
me='MAS:get_MEG_Analysis_var';
if(size(varargin,2)>1)
    error(me,'Too many input arguments!');
end
temp=getappdata(0,'MEG_Analysis_var');
if(size(varargin,2)==0)
    y=getappdata(0,'MEG_Analysis_var');
else
    switch(varargin{1})
        case 'runtimeALG'
            y=temp.runtimeALG;
        case 'ALGIndex'
            y=temp.ALGIndex;
    end
end

end

