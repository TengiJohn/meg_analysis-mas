function ini_UI_MEG_Analysis(hObject,handles)
% default setting:
% process the filtered data
setappdata(0,'MEG_Analysis',handles); 
loadALG_TEST();
set(handles.pushbutton11,'enable','off');
set(handles.pushbutton12,'enable','off');
set(handles.pushbutton13,'enable','off');
disp_alg(handles);

fighandles=get_MAS('fighandles');
fighandles.MEG_Analysis=handles;
set_MAS('fighandles',fighandles);
set_MAS('trainset',[]);
end

