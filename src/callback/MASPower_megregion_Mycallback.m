function MASPower_megregion_Mycallback( handles )
%MASPOWER_MEGREGION_MYCALLBACK Summary of this function goes here
%   Detailed explanation goes here
set(handles.pushbutton1,'enable','off');

MASPower_handles=get_MASPower_var('handles');

len=str2double(get(MASPower_handles.edit1,'string'));
if len>get_fif_info('totletime');
    len=get_fif_info('totletime');
end
begintime=str2double(get(MASPower_handles.edit3,'string'));

[datamat,times]=GET_datamat_time(begintime,len);

sensor=get_MASPower_var('sensortype');

channels=[];
top=get_MAS('topography');
if get(handles.checkbox1,'value')
    temp=cell2mat(top.lefttemporal_index);
    channels=[channels temp(sensor,:)];
end
if get(handles.checkbox2,'value')
    temp=cell2mat(top.righttemporal_index);
    channels=[channels temp(sensor,:)];
end
if get(handles.checkbox3,'value')
    temp=cell2mat(top.leftparietal_index);
    channels=[channels temp(sensor,:)];
end
if get(handles.checkbox4,'value')
    temp=cell2mat(top.rightparietal_index);
    channels=[channels temp(sensor,:)];
end
if get(handles.checkbox5,'value')
    temp=cell2mat(top.leftoccipital_index);
    channels=[channels temp(sensor,:)];
end
if get(handles.checkbox6,'value')
    temp=cell2mat(top.rightoccipital_index);
    channels=[channels temp(sensor,:)];
end
if get(handles.checkbox7,'value')
    temp=cell2mat(top.leftfrontal_index);
    channels=[channels temp(sensor,:)];
end
if get(handles.checkbox8,'value')
    temp=cell2mat(top.rightfrontal_index);
    channels=[channels temp(sensor,:)];
end

if ~isempty(channels)
    set_MASPower_var('selectedchannels',channels);
end
data=datamat(channels,:);
data=masfilter(data);

set_MASPower_var('begintime',begintime,'timelength',len,...
    'datatype','MEG','datamat',datamat,'selecteddata',data);

set(handles.pushbutton1,'enable','on');

end

