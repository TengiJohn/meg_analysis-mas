function Data_display_buttondown_Mycallback(hObject,eventdata,handles,mode)
cp=get(hObject,'CurrentPoint');
fs=get_fif_info('samplefrequency');
rt=double(get_Data_display_var('realtime'));
t=round((cp(1,1)-rt)*fs);
m=get(handles.time_slider,'Max');

if(t<1)
    t=1;
elseif(t>m)
    t=m;
end

set_Data_display_var('ptime',t);

rt=double(get_Data_display_var('realtime'));
disp_chans(handles);

switch(mode)
    case 's'
        finalRect=rbbox();
        fp=get(gca,'CurrentPoint');
        h=line([fp(1,1) fp(1,1)],get(handles.axes1,'ylim'),'color','r');
        set(h,'HitTest','off');
        
        ft=round(t+(fp(1,1)-cp(1,1))*fs);
        if(ft<1)
            ft=1;
        elseif(ft>m)
            ft=m;
        end
        t0=min(t,ft);
        t1=max(t,ft);
        if(t0==m)
            t0=m-1;
            t1=m;
        elseif(t1==t0)
            t1=t0+1;
        end
        
        set_Data_display_var('leftpoint',t0);
        rt0=(t0-1)/fs+rt;
        set(handles.text39,'string',num2str(rt0));

        set_Data_display_var('rightpoint',t1);
        rt1=(t1-1)/fs+rt;
        set(handles.text40,'string',num2str(rt1));
        % update the current train object
        begintime=rt;
        timelength=get_Data_display_var('timebuffer');
        background=get_Data_display_var('data_displaying');
       
        currenttrainobj=[];
        rawinfo.filename=get_fif_info('filename');
        rawinfo.begintime=begintime;
        rawinfo.timelength=timelength;
        rawinfo.from=t0;
        rawinfo.to=t1;
        rawinfo.channelindex=get_Data_display_var('pchannel');
        
        feature=feature_extract(t0,t1,background);
        
        currenttrainobj.rawinfo=rawinfo;
        currenttrainobj.feature=feature;
        
        set_Data_display_var('currenttrainobj',currenttrainobj);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        set_Data_display_var('leftedge',rt0);
        set_Data_display_var('rightedge',rt1);
        disp_frame(handles);

end
        
end

