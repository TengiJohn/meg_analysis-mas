% option-0 show the gray-scale image 
% option-1 show the 3-D surf image
% only display 0-100 Hz 
function disp_wigner(data,option,fs)
s=size(data);
downrate=1;
newdata=data(1:downrate:s(1),1:downrate:s(2));
s=size(newdata);
f=linspace(0,fs/2,s(1));
f=f(1:round(s(1)/10));
[X,Y]=meshgrid(0:s(2)-1,f);
amp=max(max(newdata));

flow=5;
fhigh=30;

Indexlow=0;
Indexhigh=s(1);
inif=linspace(0,fs/2,s(1));
for i=1:s(1)
    if inif(i)<flow
        Indexlow=i;
    end
    if inif(s(1)-i+1)>fhigh
        Indexhigh=s(1)-i+1;
    end
end

dispf=inif(Indexlow:Indexhigh);
dispdata=data(Indexlow:Indexhigh,:);

switch(option)
    case 'gray'
        imshow(data,[]);
    case '3d'
        surf(X,Y,newdata(1:round(s(1)/10),:)/amp)
        colormap(jet);
        xlabel('time domain (ms)');
        ylabel('frequency domain (Hz)');
        zlabel('time-frequency amplitude');
        temp=get(gca,'XTick');
        set(gca,'XTick',temp);
        set(gca,'XTickLabel',temp*downrate);
        temp=get(gca,'ZTick');
        set(gca,'ZTick',temp);
        set(gca,'ZTickLabel',temp*amp);
    case 'jet'
        imagesc(dispdata);
        set(gca,'yticklabel',{'30','25','20','15','10','5'});
        ylabel('frequency (Hz)');
        xlabel(strcat('time (ms)',' window size: ',num2str(s(1))));
        colormap(jet);
end
end

