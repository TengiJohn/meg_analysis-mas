function  [y,times] = GET_rawchannel(p)
raw=get_mne_raw();
include =[];
want_meg   = get_MAS('want_meg');
want_eeg   = get_MAS('want_eeg');
want_stim  = get_MAS('want_sti');
picks = fiff_pick_types(raw.info,want_meg,want_eeg,want_stim,include,raw.info.bads);

[B, times ] = fiff_read_raw_segment(raw,raw.first_samp,raw.last_samp,picks);

y=B(p,:);
end

