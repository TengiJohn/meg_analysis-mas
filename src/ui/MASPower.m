function varargout = MASPower(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MASPower_OpeningFcn, ...
                   'gui_OutputFcn',  @MASPower_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end
function MASPower_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
MASPower_Open_Mycallback(handles);
guidata(hObject, handles);
end
function varargout = MASPower_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;
end


function pushbutton1_Callback(hObject, eventdata, handles)
data=get_MASPower_var('selecteddata');
freqBand=get(handles.uitable1,'data');
freqs=linspace(0,500,size(data,2));
freqIndex=zeros(6,2);
for i=1:6
    indexTemp=find(freqs>freqBand(i,1)&freqs<freqBand(i,2));
    freqIndex(i,1)=indexTemp(1);
    freqIndex(i,2)=indexTemp(end);
end
power_percentage=zeros(6,size(data,1));
for i=1:size(data,1)
    temp=abs(fft(data(i,:)));
    for k=1:6
        power_percentage(k,i)=sum(temp(freqIndex(k,1):freqIndex(k,2)).^2/sum(temp(freqIndex(1,1):freqIndex(6,2)).^2));
    end
end
totle_percentage=zeros(6,1);
for i=1:6
    totle_percentage(i)=mean(power_percentage(i,:));
end
display(totle_percentage);
figure('name','barplot of frequency bands');
cla
bar(totle_percentage);
set(gca,'xticklabel',{'Band1';'Band2';'Band3';'Band4';'Band5';'Band6'});
xlabel('frequency band(Hz)');
ylabel('relative power');
end



function edit1_Callback(hObject, eventdata, handles)
end
function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end



function edit3_Callback(hObject, eventdata, handles)
end
function edit3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end



function edit4_Callback(hObject, eventdata, handles)
end
function edit4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edit5_Callback(hObject, eventdata, handles)
end
function edit5_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
function pushbutton2_Callback(hObject, eventdata, handles)
begintime=get_MASPower_var('begintime');
length=get_MASPower_var('timelength');
data=get_MASPower_var('selecteddata');

flow=str2double(get(handles.edit4,'string'));
fhigh=str2double(get(handles.edit5,'string'));

avg_stft(data,flow,fhigh,begintime,length);
end

% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)

end



function edit6_Callback(hObject, eventdata, handles)
end
function edit6_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end



function edit7_Callback(hObject, eventdata, handles)
end

function edit7_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
data=get_MASPower_var('selecteddata');
[avgfreq,avgpower]=poweranalysis(data,'avgfreq','avgpower');
avgfreq=mean(avgfreq);avgpower=mean(avgpower);
set_MASPower_var('avgfreq',avgfreq,'avgpower',avgpower);

set(handles.edit6,'string',num2str(avgfreq));
set(handles.edit7,'string',num2str(avgpower));
end


% --------------------------------------------------------------------
function uipanel10_ButtonDownFcn(hObject, eventdata, handles)

end



% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
data=get_MASPower_var('selecteddata');
flow=str2double(get(handles.edit4,'string'));
fhigh=str2double(get(handles.edit5,'string'));
avg_fft(data,flow,fhigh);
end

% --------------------------------------------------------------------
function eog_Callback(hObject, eventdata, handles)
end

% --------------------------------------------------------------------
function bipolar_Callback(hObject, eventdata, handles)
MASPower_bipolar_Mycallback(handles);
end

% --------------------------------------------------------------------
function eegspecify_Callback(hObject, eventdata, handles)
MASPower_eegspecify_Mycallback(handles);
end

function region_Callback(hObject, eventdata, handles)
MEGRegion;
end

function megspecify_Callback(hObject, eventdata, handles)
MASPower_megspecify_Mycallback(handles);
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
end


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
end
function Preference_Callback(hObject, eventdata, handles)
MASPower_Preference;
end
