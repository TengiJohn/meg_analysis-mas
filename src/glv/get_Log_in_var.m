function y= get_Log_in_var(varargin)
temp=getappdata(0,'Log_in_var');
if(size(varargin,2)==0)
    y=temp;
else
    switch varargin{1}
        case 'authen'
            y=temp.authen;
        case 'previous_user'
            y=temp.previous_user;
    end
end
end

