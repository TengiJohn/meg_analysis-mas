function varargout = Login(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Login_OpeningFcn, ...
                   'gui_OutputFcn',  @Login_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

function Login_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
ini_UI_Login(handles);
guidata(hObject, handles);
end
function varargout = Login_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;

end

function edit1_Callback(hObject, eventdata, handles)
end
function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

function edit2_Callback(hObject, eventdata, handles)
end

function edit2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function login_Callback(hObject, eventdata, handles)
Login_Mycallback(handles);
end
function pushbutton2_Callback(hObject, eventdata, handles)
end

function checkbox1_Callback(hObject, eventdata, handles)
end
function checkbox2_Callback(hObject, eventdata, handles)
end
function pushbutton3_Callback(hObject, eventdata, handles)
end

function pushbutton4_Callback(hObject, eventdata, handles)
end

function popupmenu1_Callback(hObject, eventdata, handles)
Login_user_choose_Mycallback(handles)
end
function popupmenu1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
