function previouspage(handles)
T=get_Data_display_var('realtime');
Tb=get_Data_display_var('timebuffer');
if(T-Tb<0)
    T=0;
else
    T=T-Tb;
end
set_Data_display_var('realtime',T);

B=GET_datamat_time(T,Tb);
set_Data_display_var('datamat',B);

set_Data_display_var('fdatamat',masfilter(B));

if(get(handles.checkbox1,'value')==1)
    [a,I1]=max(abs(B));
    [a,I2]=max(a);
    max_chan=I1(I2);
    set_Data_display_var('pchannel',max_chan);
end

disp_chans(handles);

temp=get_MAS('fighandles');
disp_full(temp.FullChannel_View);

end

