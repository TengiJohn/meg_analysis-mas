function ini_filter()
filter=struct();
filter.passband=[6 45];
filter.stopband=[1 70];
filter.mode=0;
filter.rp=3;
filter.rs=20;
filter.type=1;
filter.samplefrequency=1000;
filter.design='default';
filter.bpenable=1;

filter.freqcenter=50;
filter.notchmode=0;
filter.qfactor=40;
filter=calculate_filter_para(filter);
filter.notchenable=1;
set_filter(filter);
end

