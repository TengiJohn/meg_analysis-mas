function data=thresholding(data)
s=median(abs(data),2)/0.6745;
a=size(data);
for i=1:a(1)
    for j=1:a(2)
        if(abs(data(i,j))<s(i,1))
            data(i,j)=0;
        end
    end
end
end

