function y = FFT_test(data)
%short time fourier transformation
y=[];
pch=get_Data_display_var('pchannel');
t=get_Data_display_var('realtime');

len=get_Data_display_var('timebuffer');
len=get_fif_info('totletime');

[datamat,times]=GET_datamat_time(t,len);

figure;
% disp_fft(datamat(pch,:),get_fif_info('samplefrequency'),0,500);
Fs=get_fif_info('samplefrequency');
disp_fft(get_Data_display_var('data_displaying'),Fs,0,50);
% title(strcat('FFT for ',get_Data_display_var('channelname'),' at time: ',num2str(t),' : ',num2str(t+len),' s '));

end

