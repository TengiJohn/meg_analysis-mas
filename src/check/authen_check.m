function info=authen_check(name,pw,rem)
authen=loadAUTHEN();
len=size(authen,2);
for i=1:len
    if(strcmp(authen{i}.name,name)&&(strcmp(authen{i}.password,pw)||rem==1))
        info=authen{i};
    else
        info=[];
    end
end
end

