function y=get_FullChannel_View_var(varargin)
optargin=size(varargin,2);
stdargin=nargin-optargin;

var=getappdata(0,'FullChannel_View_var');
if(optargin==0)
    y=var;
else
    switch varargin{1}
        case 'leftpartition'
            y=var.leftpartition;
        case 'rightpartition'
            y=var.rightpartition;
        case 'leftindex'
            y=var.leftindex;
        case 'rightindex'
            y=var.rightindex;
    end
end

end

