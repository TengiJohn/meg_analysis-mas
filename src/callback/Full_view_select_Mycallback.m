function Full_view_select_Mycallback(hObject,eventdata,handles,axesn)
fs=get_fif_info('samplefrequency');
rt=double(get_Data_display_var('realtime'));
cp=get(hObject,'CurrentPoint');

finalRect=rbbox();
fp=get(gca,'CurrentPoint');
h=line([cp(1,1) fp(1,1) fp(1,1) cp(1,1) cp(1,1)],[cp(1,2) cp(1,2) fp(1,2) fp(1,2) cp(1,2)],'color','r');
set(h,'HitTest','off');

if(cp(1,1)>fp(1,1))
    temp=cp;
    cp=fp;
    fp=temp;
end
leftpartition=get_FullChannel_View_var('leftpartition');
rightpartition=get_FullChannel_View_var('rightpartition');
leftindex=get_FullChannel_View_var('leftindex');
rightindex=get_FullChannel_View_var('rightindex');

bpdatamat=get_Data_display_var('fdatamat');
switch axesn
    case 'axes1'
         [maxp,maxt]=search(cp,fp,rt,fs,bpdatamat,leftpartition,leftindex);
    case 'axes3'
        t0=round((cp(1,1)-rt)*fs+1);
        t1=round((fp(1,1)-rt)*fs+1);
        Amax=size(bpdatamat,2);
        if t0>Amax
            t0=Amax-1;
        end
        if t1>Amax
            t1=Amax;
        end
        
       [m,index1]=max(abs(bpdatamat(leftindex,t0:t1)));
       [m,index2]=max(m);
       
       maxp=leftindex(index1(index2));
       tr=t0:t1;
       maxt=tr(index2);
          
    case 'axes2'
        [maxp,maxt]=search(cp,fp,rt,fs,bpdatamat,rightpartition,rightindex);
    case 'axes4'
        t0=round((cp(1,1)-rt)*fs+1);
        t1=round((fp(1,1)-rt)*fs+1);
        Amax=size(bpdatamat,2);
        if t0>Amax
            t0=Amax-1;
        end
        if t1>Amax
            t1=Amax;
        end
        
        [m,index1]=max(abs(bpdatamat(rightindex,t0:t1)));
        [m,index2]=max(m);
        
        maxp=rightindex(index1(index2));
        tr=t0:t1;
        maxt=tr(index2);
end

set_Data_display_var('pchannel',maxp);
set_Data_display_var('ptime',maxt);
fig=get_MAS('fighandles');
disp_chans(fig.Data_display);

end

function [maxp,maxt]=search(cp,fp,rt,fs,bpdatamat,partition,index)
t0=round((cp(1,1)-rt)*fs+1);
t1=round((fp(1,1)-rt)*fs+1);
Amax=size(bpdatamat,2);
if t0>Amax
    t0=Amax-1;
end
if t1>Amax
    t1=Amax;
end
upindex=1;
downindex=length(index);
for i=1:length(partition)
    if partition(length(partition)-i+1,2)<cp(1,2)
        upindex=length(partition)-i+1;
    end
    if partition(i,1)>fp(1,2)
        downindex=i;
    end
end

tr=t0:t1;
chans=index(upindex:downindex);
[m,index1]=max(abs(bpdatamat(chans,t0:t1)));
[a,index2]=max(m);
if(upindex==downindex)
    maxp=chans;
    maxt=tr(index1);
else
    maxp=chans(index1(index2));
    maxt=tr(index2);
end
end

