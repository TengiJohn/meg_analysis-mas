function ALG_Remove_Mycallback(handles)
   choice=questdlg('Are you sure to remove the algorithem?','Remove','Yes','No','No');
   switch choice
       case 'Yes'
           alg=get_MAS('algorithm_test');
           num=get_MEG_Analysis_var('ALGIndex');
           alg(num,:)=[];
           set_MAS('algorithm_test',alg);
           set(handles.listbox3,'Value',1);
           disp_alg(handles);
           ALG_Select_Mycallback(handles);
       case 'No'
   end   
end

