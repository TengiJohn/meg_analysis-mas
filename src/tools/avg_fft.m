function avg_fft(data,fmin,fmax)
figure;
Fs=get_fif_info('samplefrequency');
if size(data,1)>1
    data=mean(data);
end
disp_fft(data,Fs,fmin,fmax);

end

