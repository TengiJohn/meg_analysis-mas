function y= get_fif_info(varargin)
num=size(varargin,2);
temp=getappdata(0,'Fif_Info');
if(num==0)
    y=temp;
else
    switch varargin{1}
        case 'samplefrequency'
            y=temp.samplefrequency;
        case 'filename'
            y=temp.filename;
        case 'channelnames'
            y=temp.channelnames;
        case 'totlechannel'
            y=temp.totlechannel;
        case 'totletime'
            y=temp.totletime;
        case 'MEGChannelNum'
            y=temp.MEGChannelNum;
        case 'EEGChannelNum'
            y=temp.EEGChannelNum;
        case 'MEGChannelNames'
            y=temp.MEGChannelNames;
        case 'EEGChannelNames'
            y=temp.EEGChannelNames;
    end
end
end

