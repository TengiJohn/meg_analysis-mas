function output = WT_test(data)
%wavelet transform test
output=[];

tn=get_Data_display_var('realtime');
pchannel=get_Data_display_var('pchannel');
tb=get_Data_display_var('timebuffer');
datamat=GET_datamat_time(tn,tb);

data=datamat(pchannel,:);

%energy normalization
normalConst=sqrt(sum(data.^2));
data=data/normalConst;
%
tr=linspace(tn,tn+tb,length(data));
channelname=get_Data_display_var('channelname');

scalerange=20:100;
wname='db4';
bp=bandpass(data);

figure;
[cwcoef,sc]=cwt(bp,scalerange,wname,'scal');
title('scalogram');
ylabel('Scale');

figure;
temp=zeros(size(cwcoef));
for i=1:size(cwcoef,1)
    temp(i,:)=cwcoef(i,:).^2/sum(cwcoef(i,:).^2);
end
temp1=zeros(size(cwcoef));
for i=1:size(cwcoef,2)
    temp1(:,i)=cwcoef(:,i).^2/sum(cwcoef(:,i).^2);
end
imagesc(temp);
colormap(jet);

figure;
subplot(3,1,1);
plot(tr,data);
title(strcat(channelname,' raw data'));

subplot(3,1,2);
plot(tr,bp);
title('bandpass filter');

level=7;
optlevel=4;
[c,l]=wavedec(data,level,wname);
ca=wrcoef('a',c,l,wname,optlevel);
subplot(3,1,3);
plot(tr,ca);
title(strcat('level ',num2str(optlevel),' wavelet reconstruction'));

% figure;
% temp=1;
% for i=1:level
%     if temp>5
%         figure;
%         temp=1;
%     end
%     subplot(5,1,temp);
%     datarec=wrcoef('a',c,l,wname,i);
%     plot(tr,datarec);
%     title(strcat('level ',num2str(i),' wavelet reconstruction'));
%     temp=temp+1;
% end
end

