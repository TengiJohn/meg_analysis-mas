%Smoothed none linear energy operator to detect the spike segment
%100ms per seg
%10ms  per move
function y= SNEO(x)
% omit the expectation in time domain
num=length(x);
temp=[x(1) x x(num)];
Psi=x.^2-temp(1,1:num).*temp(1,3:num+2);
%smooth in frequency domain
% output=Psi;
% width=10;
% newoutput=[zeros(1,width) output zeros(1,width)];
% y=conv(newoutput,bartlett(width));
% y=y(1,5:num+4);
%%%%%%%%%%%%%%%%%%%%%%%%%%%
y=Psi;
end

