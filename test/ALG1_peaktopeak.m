function output= ALG1_peaktopeak(varargin)
% 1��single channel algorithm
% 2,bining the data with window size 100ms
% 3,represent the bin with value (positive peak minus negtive peak) 
% 4,output structure :detail--numerical numbers classified--0,1 numbers

argin=size(varargin,2);

data=varargin{1};

if argin==1
    binwidth=100;
    bindelta=50;
elseif argin==2;
    bindelta=50;
else
    binwidth=varargin{2};
    bindelta=varargin{3};
end

if(size(data,1)==1)
    detail=ALG1_single_channel(data,binwidth,bindelta);
else

end
classified=ALG1_classifier(detail);

output.detail=detail;
output.classified=classified;
end

function y=ALG1_single_channel(data,binwidth,bindelta)
len=size(data,2);
y=zeros(1,len);
for i=1:bindelta:len
    binleft=round(i-binwidth/2);
    if(binleft<1)
        binleft=1;
    end
    
    binright=round(i+binwidth/2);
    if(binright>len);
        binright=len;
    end
    
    binrange=binleft:binright;
    peaktopeak=max(data(binrange))-min(data(binrange));
    y(binrange)=peaktopeak;
end

end

function y=ALG1_classifier(a)
len=size(a,2);
y=zeros(1,len);
%prerequisite settings
background=median(a);
threshold=1;
%%%%%%%%%%%%%%%%%%%%%%
for i=1:len
    %juding...
    if(a(i)>threshold*background)
        y(i)=1;
    else
        y(i)=0;
    end     
end
end