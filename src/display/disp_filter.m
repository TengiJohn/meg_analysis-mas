%display frequency response of filter
function disp_filter()
figure;
b=get_filter('bpnumerator');
a=get_filter('bpdenumerator');
sf=get_filter('samplefrequency');
cla
h=bodeplot(tf(b,a,1/sf));
setoptions(h,'FreqUnits','Hz');

end

