function y=calculate_filter_para(filter)
passband=filter.passband;
stopband=filter.stopband;
Rp=filter.rp;
Rs=filter.rs;
type=filter.type;
sf=filter.samplefrequency;

Wp=passband/sf*2;
Ws=stopband/sf*2;

switch type
    case 1
        [n,Wn]=buttord(Wp,Ws,Rp,Rs);
        [b,a]=butter(n,Wn);
end

filter.bpnumerator=b;
filter.bpdenumerator=a;

f0=filter.freqcenter;
q=filter.qfactor;
w0=f0/(sf/2);
bw=w0/q;
[nb,na]=iirnotch(w0,bw);
filter.notchnumerator=nb;
filter.notchdenumerator=na;

y=filter;
end

