%n is the signal train length
%r is the number of the spikes
%T is the sample interval(ms)
%num is the actual spike number generated
%s is the output signal
function [s num]= phantomsignal(n,T)
w=2*pi/(500/T);
fai=pi/4*randi([-4 4]);
baseAmp=rand*10;
if(baseAmp==0)
    baseAmp=1;
end
s=baseAmp*(sin(w*n)-sin(2*w*n+fai)+sin(4*w*n+2*fai));
pwidth=round(25/T);
% distribution=40:round(length(n)/r):length(n)-2*pwidth;
distribution=[200 400 600 800];
num=length(distribution);
pAmp=7*baseAmp;
Ampseq=randint(1,num,[-1 1]);
spike=zeros(1,2*pwidth+1);
for i=1:pwidth+1
    spike(i)=(i-1)/pwidth*pAmp;
    spike(2*pwidth+2-i)=(i-1)/pwidth*pAmp;
end

for i=1:num
    s=s+[zeros(1,distribution(i)-1) Ampseq(i)*spike zeros(1,length(n)-distribution(i)+1-length(spike))];
end
num=sum(abs(Ampseq));
end

