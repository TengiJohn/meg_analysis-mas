function y=get_Data_display_var(varargin)
optargin=size(varargin,2);
stdargin=nargin-optargin;

var=getappdata(0,'Data_display_var');
if(optargin==0)
    y=var;
else
    switch varargin{1}
        case 'timebuffer'
            y=var.timebuffer;
        case 'data_displaying'
            y=var.data_displaying;
        case 'channelname'
            y=var.channelname;
        case 'realtime'
            y=var.realtime;
        case 'datamat'
            y=var.datamat;
        case 'ptime'
            y=var.ptime;
        case 'pchannel'
            y=var.pchannel;
        case 'leftpoint'
            y=var.leftpoint;
        case 'rightpoint'
            y=var.rightpoint;
        case 'rtime'
            y=var.rtime;
        case 'dataframe'
            y=var.dataframe;
        case 'currenttrainobj'
            y=var.currenttrainobj;
        case 'trainset_data'
            y=var.trainset_data;
        case 'fdatamat'
            y=var.fdatamat;
        case 'leftedge'
            y=var.leftedge;
        case 'rightedge'
            y=var.rightedge;
        case 'frame_rtime'
            y=var.frame_rtime;
        case 'avgpower'
            y=var.avgpower;
        case 'avgfreq'
            y=var.avgfreq;
    end
end

end

