function Login_user_choose_Mycallback(handles)
temp=get_Log_in_var('previous_user');

pre_usr=temp{get(handles.popupmenu1,'value')};

set(handles.edit1,'string',pre_usr.name);
if(pre_usr.remember)
    set(handles.edit2,'string','**********');
else
    set(handles.edit2,'string','');
end

set(handles.checkbox1,'value',pre_usr.remember);
set(handles.checkbox2,'value',pre_usr.skip);

end

