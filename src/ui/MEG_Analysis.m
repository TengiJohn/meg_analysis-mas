function varargout = MEG_Analysis(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MEG_Analysis_OpeningFcn, ...
                   'gui_OutputFcn',  @MEG_Analysis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

function MEG_Analysis_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
ini_UI_MEG_Analysis(hObject,handles);
guidata(hObject, handles);
end

function varargout = MEG_Analysis_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;
end

function File_Open_Callback(hObject, eventdata, handles)
File_Open_Mycallback();
end
function File_Save_Callback(hObject,eventdata,handles)
% File_Save_Mycallback(hObject,handles);
end
       
 function File_Preference_Callback(hObject,eventdata,handles)
 Preference;
 end

function MASFilter_Callback(hObject,eventdata,handles)
Filter;
end
    
function FDATool_Callback(hObject,eventdata,handles)
fdatool;
end

function Load_FilterFile_Callback(hObject,eventdata,handles)
Load_FilterFile_Mycallback(hObject,eventdata,handles);    
end
function pushbutton10_Callback(hObject,eventdata,handles)
    ALG_Apply_Mycallback(handles);
end
function pushbutton11_Callback(hObject,eventdata,handles)
    set_Customize_var('newOrEdit','new');
    Customize
end
function pushbutton12_Callback(hObject,eventdata,handles)
ALG_Remove_Mycallback(handles);  
end
function pushbutton13_Callback(hObject,eventdata,handles)
    set_Customize_var('newOrEdit','edit');
    Customize
end

function ALGSelect_Callback(hObject,eventdata,handles)
ALG_Select_Mycallback(handles);
end

function closeRequest_Callback(hObject,eventdata,handles)
saveALG_TEST();
delete(hObject);
end

function Load_TrainSetFile_Callback(hObject,eventdata,handles)
loadTrainSet();
end

function TrainSet_Merge_Callback(hObject,eventdata,handles)
[filename,pathname,filterindex]=uigetfile({'*.tset','TrainSetfile (*.tset)'},'Select multi files to merge','multiselect','on','./database/trainset/trainset.tset');
fp=strcat(pathname,filename);
if(~isempty(filename))
    ts=ReadYaml(fp{1});
    new=[];
    if(~isempty(ts))
       new=ts.data;
    end
    if(length(fp)>1)
    for i=2:length(fp)
        temp=ReadYaml(fp{i});
        if(~isempty(temp))
            new=[new;temp.data];
        end
    end
    saveTrainSet(new);
    end
end
end
function TrainSet_View_Callback(hObject,eventdata,handles)
TrainSet_View;
end

function EEG_View_Callback(hObject,eventdata,handles)
EEG_View;
end

function Power_Callback(hObject,eventdata,handles)
MASPower;
end
function Logoff_Callback(hObject,eventdata,handles)
saveALG_TEST();
close all
Login;
end
function Exit_Callback(hObject,eventdata,handles)
close all
end