function set_filter(varargin)
num=size(varargin,2);
if(num==1)
    setappdata(0,'Filter',varargin{1});
else
    temp=getappdata(0,'Filter');
    for i=1:2:num-1
        switch varargin{i}
            case 'passband'
                temp.passband=varargin{i+1};
            case 'stopband'
                temp.stopband=varargin{i+1};
            case 'rp'
                temp.rp=varargin{i+1};
            case 'rs'
                temp.rs=varargin{i+1};
            case 'type'
                temp.type=varargin{i+1};
            case 'samplefrequency'
                temp.samplefrequency=varargin{i+1};
            case 'design'
                temp.design=varargin{i+1};
            case 'bpnumerator'
                temp.numerator=varargin{i+1};
            case 'bpdenumerator'
                temp.denumerator=varargin{i+1};
            case 'bpenable'
                temp.bpenable=varargin{i+1};
            case 'mode'
                temp.mode=varargin{i+1};
            case 'freqcenter'
                temp.freqcenter=varargin{i+1};
            case 'qfactor'
                temp.qfactor=varargin{i+1};
            case 'notchmode'
                temp.notchmode=varargin{i+1};
            case 'notchnumerator'
                temp.notchnumerator=varargin{i+1};
            case 'notchdenumerator'
                temp.notchdenumerator=varargin{i+1};
            case 'notchenable'
                temp.notchenable=varargin{i+1};
        end
    end
    setappdata(0,'Filter',temp);
end
end

