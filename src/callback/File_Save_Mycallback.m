function File_Save_Mycallback(hObject,handles)
data=getappdata(0,'Datamat');
p=getappdata(0,'Currentchannel');
rt=getappdata(0,'Realtime');
tr=getappdata(0,'Timerange');
savedata=data(p,tr);
[filename,pathname,filterindex]=uiputfile({'*.mat','Binaryfile(*.mat)';'*.txt','ASCIIfile(*.txt)'},'Save as',[num2str(p) '_' num2str(rt)]);
if(filename~=0)
    switch filterindex
        case 1
            save([pathname filename],'savedata','-mat');
        case 2
            save([pathname filename],'savedata','-ascii');
    end
end
end

