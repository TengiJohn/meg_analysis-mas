function y = notch_test( data )
y=[];
data=get_Data_display_var('data_displaying');
fs=1000;
f0=50;%frequency centroid
w0=f0/(fs/2);
Q=40;%quality factor
bw=w0/Q;%band width of the notch
[b,a]=iirnotch(w0,bw);

data1=masfilter(data);
data2=filter(b,a,data1);
figure;
disp_fft(data,1000,0,500)
title('magnitude of one-side-fft on raw data');
% set(gca,'XGrid','on');
figure;
disp_fft(data1,1000,0,500);
title('magnitude of one-side-fft after bandpass filter');
figure;
disp_fft(data2,1000,0,500);
title('magnitude of one-side-fft after notch filter');

end

