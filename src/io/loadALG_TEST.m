function y=loadALG_TEST()
user=get_MAS('user');
temp=[];
opt=user.id;
switch opt
    case 'admin'
        temp=ReadYaml('./database/user/alg_test.sys');
    case 'developer'
        temp=ReadYaml('./database/user/alg_test.custom');
end

set_MAS('algorithm_test',temp);
y=temp;
end

