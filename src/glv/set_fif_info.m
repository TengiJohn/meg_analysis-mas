function set_fif_info(varargin)
num=size(varargin,2);
if(num==1)
    setappdata(0,'Fif_Info',varargin{1});
else
    temp=getappdata(0,'Fif_Info');
    for i=1:2:num-1
        switch varargin{i}
            case 'samplefrequency'
                temp.samplefrequency=varargin{i+1};
            case 'filename'
                temp.filename=varargin{i+1};
            case 'channelnames'
                temp.channelnames=varargin{i+1};
            case 'totlechannel'
                temp.totlechannel=varargin{i+1};
            case 'totletime'
                temp.totletime=varargin{i+1};
            case 'MEGChannelNum'
                temp.MEGChannelNum=varargin{i+1};
            case 'EEGChannelNum'
                temp.EEGChannelNum=varargin{i+1};
            case 'MEGChannelNames'
                temp.MEGChannelNames=varargin{i+1};
            case 'EEGChannelNames'
                temp.EEGChannelNames=varargin{i+1};
        end
    end
    setappdata(0,'Fif_Info',temp);
end
end

