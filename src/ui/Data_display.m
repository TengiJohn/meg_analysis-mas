function varargout = Data_display(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Data_display_OpeningFcn, ...
                   'gui_OutputFcn',  @Data_display_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

function Data_display_OpeningFcn(hObject, eventdata, handles, varargin)

ini_UI_Data_display(hObject,handles);

handles.output = hObject;
guidata(hObject, handles);
end

function varargout = Data_display_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;
end

function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function time_slider_Callback(hObject, eventdata, handles)
t=round(get(hObject,'Value'));
set_Data_display_var('ptime',t);
disp_chans(handles);
end

function time_slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end

function channel_slider_Callback(hObject, eventdata, handles)
set_Data_display_var('pchannel',round(get(hObject,'Max')-get(hObject,'Value')+1));
disp_chans(handles);
end

function channel_slider_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end

function edit1_Callback(hObject, eventdata, handles)
temp=round(str2double(get(hObject,'String')));
if temp>get_fif_info('totlechannel')
    temp=get_fif_info('totlechannel');
elseif temp<1
    temp=1;
end
set(hObject,'string',num2str(temp));
set_Data_display_var('pchannel',temp);
disp_chans(handles);
temp=get_MAS('fighandles');
disp_full(temp.FullChannel_View);
end

function edit3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function pushbutton1_Callback(hObject, eventdata, handles)
nextpage(handles);
figure(handles.Data_display);
end

function pushbutton2_Callback(hObject, eventdata, handles)
previouspage(handles);
figure(handles.Data_display);
end

function edit3_Callback(hObject, eventdata, handles)
set_Data_display_var('ptime',1);
set_Data_display_var('timebuffer',str2double(get(hObject,'String')));
B=GET_datamat_time(get_Data_display_var('realtime'),get_Data_display_var('timebuffer'));
set_Data_display_var('datamat',B);

set_Data_display_var('bpdatamat',masfilter(B));

disp_chans(handles);
temp=get_MAS('fighandles');
disp_full(temp.FullChannel_View);
end

function Data_display_DeleteFcn(hObject, eventdata, handles)
end

function Data_display_CloseRequestFcn(hObject, eventdata, handles)
Close_fif();
delete(hObject);
end


function edit7_Callback(hObject, eventdata, handles)
temp=str2double(get(hObject,'String'));
tb=get_Data_display_var('timebuffer');
if temp+tb>get_fif_info('totletime')
    temp=get_fif_info('totletime')-tb;
end

set(hObject,'string',num2str(temp));
set_Data_display_var('realtime',temp);
B=GET_datamat_time(temp,get_Data_display_var('timebuffer'));
set_Data_display_var('datamat',B);

set_Data_display_var('bpdatamat',bandpass(B));

if(get(handles.checkbox1,'value')==1)
    [a,I1]=max(abs(B));
    [a,I2]=max(a);
    max_chan=I1(I2);
    set_Data_display_var('pchannel',max_chan);
end
disp_chans(handles);
temp=get_MAS('fighandles');
disp_full(temp.FullChannel_View);
end

function edit7_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function Data_display_KeyPressFcn(hObject, eventdata, handles)
Data_display_KeyPressFcn_Mycallback(hObject,eventdata,handles);
end

% --------------------------------------------------------------------
function uitoggletool6_OnCallback(hObject, eventdata, handles)
set(handles.axes1,'ButtonDownFcn',{@Data_display_buttondown_Mycallback,handles,'0'});
set(handles.uitoggletool10,'state','off');
end


% --------------------------------------------------------------------
function uitoggletool10_OnCallback(hObject, eventdata, handles)
set(handles.axes1,'ButtonDownFcn',{@Data_display_buttondown_Mycallback,handles,'s'});
set(handles.uitoggletool6,'state','off');
end


function axes2_ButtonDownFcn(hObject, eventdata, handles)
Frame_buttondown_Mycallback(hObject,handles);
end
% --------------------------------------------------------------------

function pushbutton13_Callback(hObject, eventdata, handles)
tempobj=get_Data_display_var('currenttrainobj');
selectedobj=get(handles.uipanel15,'SelectedObject');
switch(selectedobj)
    case handles.radiobutton5
        kind=1;
        temp=str2double(get(handles.text47,'string'));
        set(handles.text47,'string',num2str(round(temp+1)));
    case handles.radiobutton6
        kind=-1;
        temp=str2double(get(handles.text48,'string'));
        set(handles.text48,'string',num2str(round(temp+1)));
end
tempobj.class=kind;
ts=get_Data_display_var('trainset_data');
new=[ts;tempobj];
set(handles.text46,'string',num2str(length(new)));
set_Data_display_var('trainset_data',new);
end

function uipanel15_SelectionChangeFcn(hObject, eventdata, handles)
end

function uipushtool8_ClickedCallback(hObject, eventdata, handles)
saveTrainSet();
end

% --------------------------------------------------------------------
function uipushtool9_ClickedCallback(hObject, eventdata, handles)
saveSnapShot();
end

% --------------------------------------------------------------------
function File_Export_Callback(hObject, eventdata, handles)
chan=get_Data_display_var('channelname');
t0=get_Data_display_var('realtime');
tb=get_Data_display_var('timebuffer');
data=get_Data_display_var('data_displaying');
[filename,pathname,filterindex]=uiputfile({'*.bmp';'*.jpg'},'Save axis as',strcat('./userdata/',chan,'_',num2str(t0),'_',num2str(tb),'.bmp'));
fp=strcat(pathname,filename);
if(filename~=0)
    pix=getframe(handles.axes1);
    imwrite(pix.cdata,fp);
end
end


% --------------------------------------------------------------------
function Save_Callback(hObject, eventdata, handles)
saveTrainSet();
end

function pushbutton14_Callback(hObject, eventdata, handles)
B=get_Data_display_var('datamat');
[a,I1]=max(abs(B));
[a,I2]=max(a);
max_chan=I1(I2);
set_Data_display_var('pchannel',max_chan);

disp_chans(handles);

end

function checkbox1_Callback(hObject, eventdata, handles)
end

function pushbutton15_Callback(hObject, eventdata, handles)
left=get_Data_display_var('frame_rtime');
set_Data_display_var('leftedge',left);
right=get_Data_display_var('rightedge');
range=abs(right-left);
set(handles.text51,'string',strcat(num2str(range*1000),' ms'));
disp_frame(handles);
end

function pushbutton16_Callback(hObject, eventdata, handles)
right=get_Data_display_var('frame_rtime');
set_Data_display_var('rightedge',right);
left=get_Data_display_var('leftedge');
range=abs(right-left);
set(handles.text51,'string',strcat(num2str(range*1000),' ms'));
disp_frame(handles);
end


% --------------------------------------------------------------------
function File_ExportPreview_Callback(hObject, eventdata, handles)

t0=get_Data_display_var('realtime');
tb=get_Data_display_var('timebuffer');
data=get_Data_display_var('data_displaying');

figure('Name','Export Preview','Position',[200,200,800,300]);
plot(linspace(t0,t0+tb,length(data)),data);

end
