function varargout = MASPower_Preference(varargin)
% MASPOWER_PREFERENCE MATLAB code for MASPower_Preference.fig
%      MASPOWER_PREFERENCE, by itself, creates a new MASPOWER_PREFERENCE or raises the existing
%      singleton*.
%
%      H = MASPOWER_PREFERENCE returns the handle to a new MASPOWER_PREFERENCE or the handle to
%      the existing singleton*.
%
%      MASPOWER_PREFERENCE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MASPOWER_PREFERENCE.M with the given input arguments.
%
%      MASPOWER_PREFERENCE('Property','Value',...) creates a new MASPOWER_PREFERENCE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MASPower_Preference_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MASPower_Preference_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MASPower_Preference

% Last Modified by GUIDE v2.5 18-Jan-2014 18:37:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MASPower_Preference_OpeningFcn, ...
                   'gui_OutputFcn',  @MASPower_Preference_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MASPower_Preference is made visible.
function MASPower_Preference_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MASPower_Preference (see VARARGIN)

% Choose default command line output for MASPower_Preference
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MASPower_Preference wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MASPower_Preference_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
switch get(handles.uipanel3,'selectedobject')
    case handles.radiobutton1
        sensor=3;
    case handles.radiobutton2
        sensor=2;
    case handles.radiobutton3
        sensor=1;
end

set_MASPower_var('sensortype',sensor);
