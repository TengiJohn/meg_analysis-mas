function raw = get_mne_raw()
me='MAS:get_mne_raw';
raw=getappdata(0,'MNE_RAW');
if(isempty(raw))
    warning(me,'No data file opened!');
end
end


