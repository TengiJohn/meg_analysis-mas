%calculate the frequency centroid of signal matrix
%varargin must not be empty and can only be choosen  
%from 'autocorrelation' 'psd' 'avgfreq' 'avgpower'
function varargout = poweranalysis(sigmat,varargin)
optargin=size(varargin,2);
optargout=nargout;
if(optargin==0)
    error('Insufficient input arguments');
end

if(optargout~=optargin)
    error('The numbers of output and input arguments must be consistent!');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%using welch's method to calculate the psd of the signal
nfft=size(sigmat,2);
% sigpsd=zeros(size(sigmat,1),round(nfft/2)+1);
window=1000;
overlap=50;

Fs=get_fif_info('samplefrequency');
for i=1:size(sigmat,1)
    [sigpsd(i,:),f]=pwelch(sigmat(i,:),window,overlap,nfft,Fs,'onesided');
end

for i=1:optargin
    if strcmp(varargin{i},'avgfreq')
        avgfreq=zeros(size(sigmat,1),1);
        for j=1:size(avgfreq)
            avgfreq(j)=sum(f'.*(sigpsd(j,:)/sum(sigpsd(j,:))));
        end
        varargout{i}=avgfreq;
    elseif strcmp(varargin{i},'autocorrelation')
        autocorrelation=zeros(size(sigmat,1),2*size(sigmat,2)-1);
        for m=1:size(sigmat,1)
            autocorrelation(m,:)=xcorr(sigmat(m,:));
        end
        varargout{i}=autocorrelation;
    elseif strcmp(varargin{i},'psd')
        varargout{i}=sigpsd;
    elseif strcmp(varargin{i},'avgpower')
        avgpower=mean(sigpsd,2);
        varargout{i}=avgpower;
    elseif strcmp(varargin{i},'frequencyindex')
        varargout{i}=f;
    end
end



end

