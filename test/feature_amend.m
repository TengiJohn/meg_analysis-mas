function feature_amend(filename)
ts=ReadYaml(filename);
data=ts.data;

oldfile=data{1}.rawinfo.filename;
raw=fiff_setup_read_raw(oldfile);
for i=1:length(data)
    newfile=data{i}.rawinfo.filename;
    if ~strcmp(oldfile,newfile)
        raw=fiff_setup_read_raw(newfile);
    end
    
    t0=data{i}.rawinfo.begintime;
    buffer=data{i}.rawinfo.timelength;
    pchannel=data{i}.rawinfo.channelindex;
    datamat=GET_datamat_time(t0,buffer,raw);
    
    from=data{i}.rawinfo.from;
    to=data{i}.rawinfo.to;
    bpdatamat=bandpass(datamat);
    
    datachan=bpdatamat(pchannel,:);
    
    feature=feature_add(from,to,datachan);
    newfeature=data{i}.feature;
    
%     newfeature.rawdata=feature.rawdata;
%     newfeature.duration=feature.duration;
%     newfeature.raw_amplitude_ratio=feature.raw_amplitude_ratio;
%     newfeature.sneo_amplitude_ratio=feature.sneo_amplitude_ratio;
%     newfeature.wvd_energyratio=feature.wvd_energyratio;
%     newfeature.sneo_amplitude_ratio_mean=feature.sneo_amplitude_ratio_mean;
    newfeature.sneo_amplitude_ratio_apex=feature.sneo_amplitude_ratio_apex;
    data{i}.feature=newfeature;
    oldfile=newfile;
end
ts.data=data;
WriteYaml(filename,ts);

end

function feature=feature_add(from,to,data)
%time domain
[m,index]=max(abs(data(from:to)));

index=index+from-1;
rawdata=data(index-100:index+100);

left=index-1;
right=index+1;
temp1=-1;
temp2=-1;
while temp1<=0||temp2<=0

    temp1=(data(left)-data(left-1))*(data(left)-data(left+1));
    temp2=(data(right)-data(right-1))*(data(right)-data(right+1));
    if temp1<=0
    left=left-1;
    end
    if temp2<=0
    right=right+1;
    end
end

duration=right-left;
positive=data(data>0);
negative=data(data<0);
temp1=abs(data(index)-data(left));
temp2=abs(data(index)-data(right));

raw_amplitude_ratio=max(temp1,temp2)/(median(positive)-median(negative));

%SNEO
sneo_data=SNEO(data);
positive=sneo_data(sneo_data>0);
negative=sneo_data(sneo_data<0);
temp1=abs(sneo_data(index)-sneo_data(left));
temp2=abs(sneo_data(index)-sneo_data(right));


sneo_amplitude_ratio=max(temp1,temp2)/(median(positive)-median(negative));
sneo_amplitude_ratio_mean=max(temp1,temp2)/(mean(positive)-mean(negative));

max_apx=sort(positive(find(diff(sign(diff(positive)))<0)+1));
min_apx=sort(negative(find(diff(sign(diff(negative)))>0)+1));

pback=max_apx(end-10);
nback=min_apx(10);
sneo_amplitude_ratio_apex=max(temp1,temp2)/(pback-nback);

feature.sneo_amplitude_ratio_apex=sneo_amplitude_ratio_apex;
feature.sneo_amplitude_ratio_mean=sneo_amplitude_ratio_mean;
feature.rawdata=rawdata;
feature.duration=duration;
feature.raw_amplitude_ratio=raw_amplitude_ratio;
feature.sneo_amplitude_ratio=sneo_amplitude_ratio;
 

%wigner distribution
w=flipud(abs(wigner(data)));
wvd_energyratio=sum(sum(w(:,from:to).^2))/sum(sum(w.^2));
feature.wvd_energyratio=wvd_energyratio;
end

