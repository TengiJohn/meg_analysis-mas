function ini_UI_FullChannel_View(handles)
temp=get_MAS('fighandles');
temp.FullChannel_View=handles;
set_MAS('fighandles',temp);

top=ReadYaml('./database/topography/brainregion.top');
set_MAS('topography',top);

set(handles.axes1,'ButtonDownFcn',{@Full_view_select_Mycallback,handles,'axes1'});
set(handles.axes2,'ButtonDownFcn',{@Full_view_select_Mycallback,handles,'axes2'});
set(handles.axes3,'ButtonDownFcn',{@Full_view_select_Mycallback,handles,'axes3'});
set(handles.axes4,'ButtonDownFcn',{@Full_view_select_Mycallback,handles,'axes4'});

disp_full(handles);
end

