function Filter_configure_Mycallback(hObject,handles)
set_filter('mode',0,'passband',[str2double(get(handles.edit1,'String')) str2double(get(handles.edit3,'String'))]);
set_filter('stopband',[str2double(get(handles.edit5,'String')) str2double(get(handles.edit6,'String'))]);
set_filter('rp',str2double(get(handles.edit7,'String')));
set_filter('rs',str2double(get(handles.edit8,'String')));
set_filter('notchenable',get(handles.radiobutton4,'Value'),'bpenable',get(handles.radiobutton5,'Value'));
set_filter('freqcenter',str2double(get(handles.edit9,'String')),'qfactor',str2double(get(handles.edit10,'String')));
set_filter('notchmode',get(handles.radiobutton2,'value'));
filter=calculate_filter_para(get_filter());
set_filter(filter);
guidata(hObject,handles);
end

