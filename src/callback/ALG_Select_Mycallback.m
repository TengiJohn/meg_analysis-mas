function ALG_Select_Mycallback(handles)
alglist=get_MAS('algorithm_test');
num=get(handles.listbox3,'Value');
temp{1}=['Name:          ' alglist{num}.name];
temp{2}=['Comment:     ' alglist{num}.comment];
temp{3}=['Author:        ' alglist{num}.author];
temp{4}=['Date:           ' alglist{num}.date];
set(handles.text5,'String',temp);
set_MEG_Analysis_var('runtimeALG',alglist{num});
set_MEG_Analysis_var('ALGIndex',num);

set(handles.pushbutton11,'enable','on');
set(handles.pushbutton12,'enable','on');
set(handles.pushbutton13,'enable','on');
end

