function y = wigner_sneo(data)
y=[];
Fs=get_fif_info('samplefrequency');
fdata=get_Data_display_var('data_displaying');
tr=1:size(fdata,2);

w=flipud(abs(wigner(fdata)));

figure('Name','SNEO');
subplot(2,1,1);
plot(tr,fdata);
xlim([tr(1),tr(end)]);
title('Displayed data');

subplot(2,1,2)
plot(tr,SNEO(fdata));
xlim([tr(1),tr(end)]);
title('SNEO:');

figure;
disp_wigner(w,'jet',Fs);

end

