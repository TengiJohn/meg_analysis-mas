function Login_Mycallback(handles)
info=authen_check(get(handles.edit1,'string'),get(handles.edit2,'string'),get(handles.checkbox1,'value'));
if(isempty(info))
    msgbox('Access denied due to invalid username or password!','Login','error');
else
    set(gcf,'Visible','off');
    temp=loadUSER();
    flag=0;
    for i=1:length(temp)
       if(~isempty(temp{i}))
           if(strcmp(temp{i}.name,info.name))
               temp{i}.remember=get(handles.checkbox1,'value');
               temp{i}.skip=get(handles.checkbox2,'value');
               flag=1;
           end
       end
    end
    newuser=temp;
    if(~flag)
        newuser{1}.name=info.name;
        newuser{1}.passward=info.passward;
        newuser{1}.remember=get(handles.checkbox1,'value');
        newuser{1}.skip=get(handles.checkbox2,'value');
        
        for i=2:length(temp)
            newuser{i}=temp{i-1};
        end
    end
    saveUSER(newuser);
    set_MAS('user',info);
    MEG_Analysis;
end
end


