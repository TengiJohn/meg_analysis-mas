function varargout = FullChannel_View(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FullChannel_View_OpeningFcn, ...
                   'gui_OutputFcn',  @FullChannel_View_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

function FullChannel_View_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
ini_UI_FullChannel_View(handles);
guidata(hObject, handles);
end
function varargout = FullChannel_View_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;
end

function pushbutton1_Callback(hObject, eventdata, handles)
temp=get_MAS('fighandles');
previouspage(temp.Data_display);
end
function pushbutton2_Callback(hObject, eventdata, handles)
temp=get_MAS('fighandles');
nextpage(temp.Data_display);
end

function uipanel1_SelectionChangeFcn(hObject, eventdata, handles)
disp_full(handles);
end


% --- Executes when selected object is changed in uipanel20.
function uipanel20_SelectionChangeFcn(hObject, eventdata, handles)
disp_full(handles);
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
disp_full(handles);
end


% --------------------------------------------------------------------
function uipanel1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to uipanel1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end
