function [w,b]= testSNEO()
range=1:1000;
[a b]=phantomsignal(range,1);
a=cos(2*pi/10*range)+cos(2*pi/5*range+1);
b=0;
subplot(3,1,1)
plot(range,a);
title('The simulating singal');
subplot(3,1,2)
plot(range,SNEO(a));
title('The NEO output');
subplot(3,1,3)
plot(range,sharp(a));

data=abs(wigner(a,1));
figure();
disp_tf(flipud(data),0,1000);

figure();
disp_tf(data,1,1000);
w=data;
end

