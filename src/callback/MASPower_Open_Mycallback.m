function MASPower_Open_Mycallback( handles )
freqBand=zeros(6,2);
freqBand(1,1)=0.5; freqBand(1,2)=3;
freqBand(2,1)=4;   freqBand(2,2)=8;
freqBand(3,1)=9;   freqBand(3,2)=10;
freqBand(4,1)=10;  freqBand(4,2)=13;
freqBand(5,1)=14;  freqBand(5,2)=18;
freqBand(6,1)=22;  freqBand(6,2)=48;
set(handles.uitable1,'data',freqBand);


set_MASPower_var('handles',handles);
set_MASPower_var('sensortype',3);

end

