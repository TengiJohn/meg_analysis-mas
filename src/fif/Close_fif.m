function Close_fif()
raw=get_mne_raw();
if(isempty(raw))
    return;
end
fclose(raw.fid);
set_mne_raw([]);
end

