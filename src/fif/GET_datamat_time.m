function [datamat,times]= GET_datamat_time(varargin) %time unit: s
%varargin{1}--begin 
%varargin{2}--length
%varargin{3}--raw
if(size(varargin,2)~=3)
    raw=get_mne_raw();
else
    raw=varargin{3};
end
include =[];
want_meg   = get_MAS('want_meg');
want_eeg   = get_MAS('want_eeg');
want_stim  = get_MAS('want_sti');

picks = fiff_pick_types(raw.info,want_meg,want_eeg,want_stim,include,raw.info.bads);

if(size(varargin,2)==0)
    [datamat, times ] = fiff_read_raw_segment(raw,raw.first_samp,raw.last_samp,picks);
elseif(size(varargin,2)==2||size(varargin,2)==3)
    from=varargin{1};
    to=varargin{2}+from;
    from=from+raw.first_samp/raw.info.sfreq;
    to=to+raw.first_samp/raw.info.sfreq;
    [datamat,times] =  fiff_read_raw_segment_times(raw,from,to,picks);
end

megnum=get_fif_info('MEGChannelNum');
eegnum=get_fif_info('EEGChannelNum');
for i=1:3:megnum
    datamat(i,:)=datamat(i,:)*10^12;
    datamat(i+1,:)=datamat(i+1,:)*10^12;
    datamat(i+2,:)=datamat(i+2,:)*10^12;
end

for i=megnum+1:megnum+eegnum
    datamat(i,:)=datamat(i,:)*10^6;
end

end

