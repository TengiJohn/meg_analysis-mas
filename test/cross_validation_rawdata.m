function cross_validation_rawdata()

[filename,pathname,filterindex]=uigetfile({'*.tset','TrainSetfile (*.tset)'},'Select the first file as training set','MultiSelect','off','./database/trainset/trainset_0.tset');
fp=strcat(pathname,filename);
temp=[];
if(filename~=0)
    switch filterindex
        case 1
            temp=ReadYaml(fp);
    end
end

fullset=temp.data;
totlenum=length(fullset);
delta=floor(totlenum/4);
 windowrange=50:150;
out=zeros(length(windowrange),3);
for m=1:4
    statistic_data=[];
    
    testrange=(m-1)*delta+1:m*delta;
    trainrange=[1:(m-1)*delta m*delta:totlenum];
    
    trainingset=fullset(trainrange);
    
    testingset=fullset(testrange);
   
    for window=windowrange
    traininglabel=zeros(length(trainingset),1);
    for i=1:length(trainingset)
        if trainingset{i}.class==1
            traininglabel(i)=1;
        else
            traininglabel(i)=0;
        end
        f=trainingset{i}.feature;
        testrange=round(100-window/2):round(100+window/2);
        rawdata=f.rawdata;
        trainingfeature(i,:)=rawdata{testrange};
    end
    
    testinglabel=zeros(length(testingset),1);
    for i=1:length(testingset)
        if testingset{i}.class==1
            testinglabel(i)=1;
        else
            testinglabel(i)=0;
        end
        f=testingset{i}.feature;
        testrange=round(100-window/2):round(100+window/2);
        rawdata=f.rawdata;
        testingfeature(i,:)=rawdata{testrange};
    end
    
    svmstruct=svmtrain(trainingfeature,traininglabel);
    
    pre_testinglabel=svmclassify(svmstruct,testingfeature);
    
    num_falsepositive=0;
    num_falsenegative=0;
    num_truepositive=0;
    num_truenegative=0;
    
    num_testing=length(testinglabel);
    num_testingpositive=sum(testinglabel);
    num_testingnegative=num_testing-num_testingpositive;
    
    for i=1:num_testing
        if testinglabel(i)==1&&pre_testinglabel(i)==0
            num_falsenegative=num_falsenegative+1;
        end
        
        if testinglabel(i)==0&&pre_testinglabel(i)==1
            num_falsepositive=num_falsepositive+1;
        end
        
        if testinglabel(i)==1&&pre_testinglabel(i)==1
            num_truepositive=num_truepositive+1;
        end
        
        if testinglabel(i)==0&&pre_testinglabel(i)==0
            num_truenegative=num_truenegative+1;
        end
    end
    
    output.percent_training_positive=sum(traininglabel)/length(traininglabel);
    output.percent_training_negative=1-output.percent_training_positive;
    
    output.percent_falsepositive=num_falsepositive/num_testingnegative;
    output.percent_sensitive=num_truepositive/num_testingpositive;
    
    statistic_data=[statistic_data;output.percent_falsepositive  output.percent_sensitive 1-output.percent_sensitive];
    end
    out=statistic_data+out;
end
    final=out/4;
    
    plot(windowrange,final(:,1),'color','r');
    hold on;
    plot(windowrange,final(:,3),'color','g');
end


