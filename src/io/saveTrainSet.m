function saveTrainSet(varargin)
temp=get_MAS('user');
user.name=temp.name;
user.id=temp.id;

if(size(varargin,2)==0)
    data=get_Data_display_var('trainset_data');
elseif(size(varargin,2)==1)
    data=varargin{1};
end

output.user=user;
output.data=data;

nt=clock;
output.savetime=nt;

num=length(data);
[filename,pathname,filterindex]=uiputfile({'*.tset','TrainSetfile(*.tset)'},'Save training set as',strcat('./database/trainset/','trainset','_',num2str(num)));
fp=strcat(pathname,filename);
if(filename~=0)
    switch filterindex
        case 1
            WriteYaml(fp,output);
    end
end

end

